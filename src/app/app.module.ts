import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule, Routes } from "@angular/router";
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatTableModule } from "@angular/material/table";
import { TranslateModule } from "@ngx-translate/core";
import { FormsModule } from "@angular/forms";
import { FuseModule } from "@fuse/fuse.module";
import { FuseSharedModule } from "@fuse/shared.module";
import {
    FuseProgressBarModule,
    FuseSidebarModule,
    FuseThemeOptionsModule,
} from "@fuse/components";

import { fuseConfig } from "app/fuse-config";

import { AppComponent } from "app/app.component";
import { LayoutModule } from "app/layout/layout.module";
import { BancosComponent } from "./main/Administrador/bancos/bancos.component";
import { AgenciasComponent } from "./main/Administrador/bancos/agencias/agencias.component";
import { LoginComponent } from "./main/login/login.component";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MaterialFileInputModule } from "ngx-material-file-input";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatSortModule } from "@angular/material/sort";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatDialogModule } from "@angular/material/dialog";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTabsModule } from "@angular/material/tabs";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { MatDividerModule } from "@angular/material/divider";
import { FuseWidgetModule } from "@fuse/components/widget/widget.module";

import {
    CalendarModule as AngularCalendarModule,
    DateAdapter,
} from "angular-calendar";
import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
import { InMemoryWebApiModule } from "angular-in-memory-web-api";
import { EmpleadosComponent } from "./main/Administrador/empleados/empleados.component";
import { BeneficiariosComponent } from "./main/Administrador/beneficiarios/beneficiarios.component";
import { DistritosComponent } from "./main/Administrador/distritos/distritos.component";
import { IncidentesComponent } from "./main/Administrador/incidentes/incidentes.component";
import { IncidenciaFormComponent } from "./main/Administrador/incidentes/incidente-form/incidente-form.component";
import { BeneficiariosEmpComponent } from "./main/Empleado/beneficiarios-emp/beneficiarios-emp.component";
import { IncidentesEmpComponent } from "./main/Empleado/incidentes-emp/incidentes-emp.component";
import { BeneficiarioMasivoComponent } from "./main/Administrador/beneficiarios/beneficiario_masivo/beneficiario-masivo.component";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatRippleModule } from "@angular/material/core";
import { MessageBoxComponent } from "./shared/message-box/message-box.component";
import { EmpleadoFormComponent } from "./main/Administrador/empleados/empleado-form/empleado-form.component";
import { BancoAutocompleteComponent } from "./main/Administrador/empleados/empleado-form/banco-autocomplete/banco-autocomplete.component";
import { MatSelectModule } from "@angular/material/select";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatCardModule } from "@angular/material/card";
import { CalendarioComponent } from "./main/Administrador/cronograma/calendario/calendario.component";
import { BeneficiariosHoraComponent } from "./main/Administrador/cronograma/calendario/beneficiarios-hora/beneficiarios-hora.component";
import { CronogramaFormComponent } from "./main/Administrador/cronograma/cronograma-form/cronograma-form.component";
import { EmpleadoMasivoComponent } from "./main/Administrador/empleados/empleado-masivo/empleado-masivo.component";
import { LobbyComponent } from "./main/Beneficiario/lobby/lobby.component";
import { ConsultaComponent } from "./main/Beneficiario/consulta/consulta.component";
import { SelectAgenciaFormComponent } from "./main/Administrador/cronograma/calendario/select-agencia-form/select-agencia-form.component";
import { CalendarioService } from "./services/calendario.service";
import { DatePipe } from "@angular/common";
import { HorarioComponent } from "./main/Beneficiario/horario/horario.component";
import { BancoFormComponent } from "./main/Administrador/bancos/banco-form/banco-form.component";
import { DistritoFormComponent } from "./main/Administrador/distritos/distrito-form/distrito-form.component";
import { CronogramaComponent } from "./main/Administrador/cronograma/cronograma.component";
import { ReportesComponent } from "./main/Administrador/reportes/reportes.component";
import { AgenciaFormComponent } from "./main/Administrador/bancos/agencias/agencia-form/agencia-form.component";
import { AgenciaMasivoComponent } from "./main/Administrador/bancos/agencias/agencia-masivo/agencia-masivo.component";
import { registerLocaleData } from "@angular/common";
import localeEs from "@angular/common/locales/es";
import { ConfiguracionFormComponent } from "./main/Administrador/cronograma/configuracion-form/configuracion-form.component";
import { DistritoMasivoComponent } from "./main/Administrador/distritos/distrito-masivo/distrito-masivo.component";
import { IncidenteEmpFormComponent } from "./main/Empleado/incidentes-emp/incidente-emp-form/incidente-emp-form.component";
import { AuthAdminGuard } from "./services/auth-admin.guard";
import { AuthEmpleadoGuard } from "./services/auth-empleado.guard";
import { ChartsModule } from "ng2-charts";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { CajeroComponent } from './main/Cajero/cajero/cajero.component';
import { CobroComponent } from './main/Cajero/cobro/cobro/cobro.component';

registerLocaleData(localeEs);

const appRoutes: Routes = [
    {
        path: "",
        redirectTo: "main",
        pathMatch: "full",
    },
    {
        path: "main",
        component: LobbyComponent,
    },
    {
        path: "cajero",
        component: CajeroComponent,
    },
    {
        path: "cajero/cobro",
        component: CobroComponent,
    },
    {
        path: "beneficiario/consultar",
        component: ConsultaComponent,
    },
    {
        path: "beneficiario/horario",
        component: HorarioComponent,
    },
    {
        path: "auth",
        component: LoginComponent,
    },
    {
        path: "admin/bancos",
        component: BancosComponent,
        canActivate: [AuthAdminGuard],
    },
    {
        path: "admin/agencias/:id",
        component: AgenciasComponent,
        canActivate: [AuthAdminGuard],
    },
    {
        path: "admin/empleados",
        component: EmpleadosComponent,
        canActivate: [AuthAdminGuard],
    },
    {
        path: "admin/beneficiarios",
        component: BeneficiariosComponent,
        canActivate: [AuthAdminGuard],
    },
    {
        path: "admin/cronograma",
        component: CronogramaComponent,
        canActivate: [AuthAdminGuard],
    },
    {
        path: "admin/calendario",
        component: CalendarioComponent,
        canActivate: [AuthAdminGuard],
    },
    {
        path: "admin/reportes",
        component: ReportesComponent,
        canActivate: [AuthAdminGuard],
    },
    {
        path: "admin/distritos",
        component: DistritosComponent,
        canActivate: [AuthAdminGuard],
    },
    {
        path: "admin/incidentes",
        component: IncidentesComponent,
        canActivate: [AuthAdminGuard],
    },
    {
        path: "empleado/beneficiarios",
        component: BeneficiariosEmpComponent,
        canActivate: [AuthEmpleadoGuard],
    },
    {
        path: "empleado/incidentes",
        component: IncidentesEmpComponent,
        canActivate: [AuthEmpleadoGuard],
    },
    {
        path: "**",
        redirectTo: "auth",
    },
];

@NgModule({
    declarations: [
        AppComponent,
        BancosComponent,
        AgenciasComponent,
        LoginComponent,
        BancoFormComponent,
        CronogramaComponent,
        ReportesComponent,
        AgenciaFormComponent,
        AgenciaMasivoComponent,
        EmpleadosComponent,
        BeneficiariosComponent,
        DistritosComponent,
        DistritoFormComponent,
        IncidentesComponent,
        IncidenciaFormComponent,
        BeneficiariosEmpComponent,
        IncidentesEmpComponent,
        MessageBoxComponent,
        EmpleadoFormComponent,
        BancoAutocompleteComponent,
        BeneficiarioMasivoComponent,
        CalendarioComponent,
        BeneficiariosHoraComponent,
        CronogramaFormComponent,
        EmpleadoMasivoComponent,
        LobbyComponent,
        ConsultaComponent,
        SelectAgenciaFormComponent,
        HorarioComponent,
        ConfiguracionFormComponent,
        DistritoMasivoComponent,
        IncidenteEmpFormComponent,
        CajeroComponent,
        CobroComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatButtonModule,
        MatMenuModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatDialogModule,
        MatToolbarModule,
        MaterialFileInputModule,
        MatSnackBarModule,
        MatRippleModule,
        MatAutocompleteModule,
        MatSelectModule,
        MatGridListModule,
        MatCardModule,
        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule,
        MatSlideToggleModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,

        NgxChartsModule,
        ChartsModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,

        AngularCalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        MatSelectModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
    ],
    providers: [DatePipe],
    bootstrap: [AppComponent],
})
export class AppModule {}
