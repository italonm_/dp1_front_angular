import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { LoginService } from 'app/services/login.service';

@Component({
  selector: 'app-cambiar-contrasena',
  templateUrl: './cambiar-contrasena.component.html',
  styleUrls: ['./cambiar-contrasena.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations,
  providers: [MatSnackBar],
})
export class CambiarContrasenaComponent implements OnInit {
  form: FormGroup;
  hideP1 = true;
  hideP2 = true;
  hideP3 = true;
  noMatch = false;

  constructor(
    public matDialogRef: MatDialogRef<CambiarContrasenaComponent>,
    private loginService: LoginService,
    private _formBuilder: FormBuilder,
    private _matSnackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      password: ["", Validators.required],
      newPassword: ["", Validators.required],
      newPasswordConfirm: ["", Validators.required],
    });
  }

  changePassword(): void {
    let form = this.form.value; 
    if (form.newPassword === form.newPasswordConfirm){
      let user = {
        username: localStorage.getItem("username"),
        password: form.password
      }
      let newUser = {
        username: localStorage.getItem("username"),
        contrasena: form.newPassword
      }
      console.log(user,newUser)
      this.loginService.changePassword(user, newUser).subscribe(
        (res) => {
            this._matSnackBar.open("Grabación exitosa", "", {
                verticalPosition: "top",
                duration: 5000,
            });
            this.matDialogRef.close();
        },
        (err) => {
            this._matSnackBar.open("Error, contraseña no actualizada", "", {
                verticalPosition: "top",
                duration: 5000,
            });
        });
      }
       else {
        this._matSnackBar.open("La nueva contraseña y su confirmación deben coincidir", "", {
          verticalPosition: "top",
          duration: 5000,
          panelClass: "red-snackbar",
        });
       }
    }
}
