import { NgModule } from '@angular/core';

import { VerticalLayout1Module } from 'app/layout/vertical/layout-1/layout-1.module';
import { VerticalLayout2Module } from 'app/layout/vertical/layout-2/layout-2.module';
import { VerticalLayout3Module } from 'app/layout/vertical/layout-3/layout-3.module';

import { HorizontalLayout1Module } from 'app/layout/horizontal/layout-1/layout-1.module';
import { CambiarContrasenaComponent } from './components/cambiar-contrasena/cambiar-contrasena.component';

import { MatIconModule } from "@angular/material/icon";
import { MatFormFieldModule } from "@angular/material/form-field";
import { ReactiveFormsModule } from '@angular/forms';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatInputModule } from "@angular/material/input";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatMenuModule } from "@angular/material/menu";

@NgModule({
    imports: [
        VerticalLayout1Module,
        VerticalLayout2Module,
        VerticalLayout3Module,

        HorizontalLayout1Module,

        MatIconModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        FuseSharedModule,
        MatInputModule,
        MatToolbarModule,
        MatButtonModule,
        MatMenuModule
    ],
    exports: [
        VerticalLayout1Module,
        VerticalLayout2Module,
        VerticalLayout3Module,

        HorizontalLayout1Module,

        MatIconModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        FuseSharedModule,
        MatInputModule,
        MatToolbarModule,
        MatButtonModule,
        MatMenuModule

    ],
    declarations: [CambiarContrasenaComponent]
})
export class LayoutModule
{
}
