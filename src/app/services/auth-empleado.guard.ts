import { Injectable } from "@angular/core";
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class AuthEmpleadoGuard implements CanActivate {
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        return sessionStorage.getItem("perfil") === "empleado";
    }

    status(): boolean {
        return sessionStorage.getItem("perfil") === "empleado";
    }

    login(): void {
        sessionStorage.setItem("perfil", "empleado");
    }

    logout(): void {
        sessionStorage.removeItem("perfil");
    }
}
