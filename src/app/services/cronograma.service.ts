import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { HOST } from "app/shared/constant";

@Injectable({
    providedIn: "root",
})
export class CronogramaService {
    constructor(private http: HttpClient) {}

    listar(): Observable<any> {
        return this.http.get<any[]>(HOST + `/configuracion`);
    }

    consultar(id: number): Observable<any> {
        return this.http.get<any>(HOST + `/configuracion/${id}`);
    }

    consultarConfiguracion(): Observable<any> {
        return this.http.get<any>(HOST + `/configuracionactual`);
    }

    actualizarConfiguracion(configuracion: any): Observable<any> {
        return this.http.post(HOST + `/configuracionactual`, configuracion);
    }

    crear(cronograma: any): Observable<any> {
        return this.http.post(HOST + `/configuracion`, cronograma, {
            responseType: "blob",
        });
    }
}
