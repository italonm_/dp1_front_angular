import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AgenciaModel } from "app/models/agencia.model";
import { EmpleadoModel } from "app/models/empleado.model";
import { HOST } from "app/shared/constant";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class EmpleadoService {
    constructor(private http: HttpClient) {}

    listar(): Observable<any> {
        return this.http.get<EmpleadoModel[]>(HOST + `/usuario/empleado`);
    }

    listarAgenciasPorBanco(idBanco: number): Observable<any> {
      return this.http.get<AgenciaModel[]>(
          HOST + `/lugarcobro/${idBanco}/agencias`
      );
    }

    consultar(id: string): Observable<any> {
        return this.http.get<EmpleadoModel>(HOST + `/usuario/${id}`);
    }

    crear(empleado: object): Observable<any> {
        return this.http.post(HOST + `/credencial`, empleado);
    }

    editar(idUsuario: string, empleado: object): Observable<any> {
        return this.http.put(HOST + `/usuario/${idUsuario}`, empleado);
    }

    eliminar(empleado: EmpleadoModel): Observable<any> {
        return this.http.put(
            HOST + `/usuario/eliminar/${empleado.idUsuario}`,
            empleado
        );
    }

    crearMasivo(file: any): Observable<any> {
        return this.http.post(HOST + `/csv/2`, file, {
            responseType: "blob",
        });
    }

    descargar(): Observable<any> {
        return this.http.post(HOST + `/csv/plantilla/2`, "zzz", {
            responseType: "blob",
        });
    }
}
