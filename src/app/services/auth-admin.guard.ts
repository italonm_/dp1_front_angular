import { Injectable } from "@angular/core";
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class AuthAdminGuard implements CanActivate {
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        return sessionStorage.getItem("perfil") === "admin";
    }

    status(): boolean {
        return sessionStorage.getItem("perfil") === "admin";
    }

    login(): void {
        sessionStorage.setItem("perfil", "admin");
    }

    logout(): void {
        sessionStorage.removeItem("perfil");
    }
}
