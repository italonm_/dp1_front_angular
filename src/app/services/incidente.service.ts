import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { IncidenteModel } from "app/models/incidente.model";
import { MotivoIncidenciaModel } from "app/models/motivoIncidencia.model";
import { HOST } from "app/shared/constant";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class IncidenteService {
    constructor(private http: HttpClient) {}

    listar(lugarCobro: any): Observable<any> {
        return this.http.get<IncidenteModel[]>(
            HOST + `/incidencia/lugarCobro/${lugarCobro}`
        );
    }

    listarMotivos(tipo: string): Observable<any> {
        return this.http.get<MotivoIncidenciaModel[]>(
            HOST + `/motivoIncidencia/${tipo}`
        );
    }

    consultar(id: number): Observable<any> {
        return this.http.get<IncidenteModel>(HOST + `/incidencia/${id}`);
    }

    crear(incidencia: IncidenteModel, tipo: number): Observable<any> {
        return this.http.post(HOST + `/incidencia/${tipo}`, incidencia);
    }

    editar(incidencia: IncidenteModel, tipo: number): Observable<any> {
        return this.http.put(
            HOST + `/incidencia/editar/${incidencia.idIncidencia}/${tipo}`,
            incidencia
        );
    }

    eliminar(incidencia: IncidenteModel, tipo: number): Observable<any> {
        return this.http.put(
            HOST + `/incidencia/eliminar/${incidencia.idIncidencia}/${tipo}`,
            incidencia
        );
    }
}
