import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {
    BeneficiarioModel,
    BeneficiarioAgenciaModel,
    BeneficiarioModelAux,
} from "app/models/beneficiario.model";
import { HOST } from "app/shared/constant";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class BeneficiarioService {
    constructor(private http: HttpClient) {}

    listar(pageIndex: number, pageSize: number): Observable<any> {
        return this.http.get<BeneficiarioModelAux[]>(
            HOST + `/beneficiario?size=${pageSize}&page=${pageIndex}`
        );
    }

    consultar(id: number): Observable<any> {
        return this.http.get<BeneficiarioModel>(HOST + `/beneficiario/${id}`);
    }
    //carga masiva
    crearMasivo(file: any): Observable<any> {
        return this.http.post(HOST + `/csv/1`, file, {
            responseType: "blob",
        });
    }
    //beneficiarios de cierta agencia

    listarPorAgencia(id: string): Observable<any> {
        return this.http.get<BeneficiarioAgenciaModel[]>(
            HOST + `/cronograma/dia/${id}`
        );
    }

    descargar(): Observable<any> {
        return this.http.post(HOST + `/csv/plantilla/1`, "zzz", {
            responseType: "blob",
        });
    }

    asistir(idConfiguracion: string, idBeneficiario): Observable<any> {
        return this.http.put(
            HOST + `/cronograma/cobrado/${idConfiguracion}/${idBeneficiario}`,
            "zzz"
        );
    }
}
