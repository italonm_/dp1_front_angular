import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HOST } from "app/shared/constant";
import { Observable } from "rxjs";
import { DistritoModel } from "app/models/distrito.model";

@Injectable({
    providedIn: "root",
})
export class DistritoService {
    constructor(private http: HttpClient) {}

    listar(): Observable<any> {
        return this.http.get<DistritoModel[]>(HOST + `/distrito`);
    }

    consultar(id: number): Observable<any> {
        return this.http.get<DistritoModel>(HOST + `/distrito/${id}`);
    }

    crear(distrito: DistritoModel): Observable<any> {
        return this.http.post(HOST + `/distrito`, distrito);
    }

    editar(distrito: DistritoModel): Observable<any> {
        return this.http.put(
            HOST + `/distrito/${distrito.idDistrito}`,
            distrito
        );
    }

    eliminar(distrito: DistritoModel): Observable<any> {
        return this.http.put(
            HOST + `/distrito/eliminar/${distrito.idDistrito}`,
            distrito
        );
    }

    crearMasivo(file: any): Observable<any> {
        return this.http.post(HOST + `/csv/3`, file, {
            responseType: "blob",
        });
    }

    descargar(): Observable<any> {
        return this.http.post(HOST + `/csv/plantilla/3`, "zzz", {
            responseType: "blob",
        });
    }
}
