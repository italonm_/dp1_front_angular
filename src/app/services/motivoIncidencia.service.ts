import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MotivoIncidenciaModel } from 'app/models/motivoIncidencia.model';
import { HOST } from "app/shared/constant";
import { Observable } from 'rxjs';

@Injectable({
    providedIn: "root",
})
export class IncidenciaService {
    constructor(private http: HttpClient) { }

    listar(): Observable<any> {
        return this.http.get<MotivoIncidenciaModel[]>(HOST + `/motivoIncidencia`);
    }

    consultar(id: number): Observable<any> {
        return this.http.get<MotivoIncidenciaModel>(HOST + `/motivoIncidencia/${id}`);
    }

    crear(incidencia: MotivoIncidenciaModel): Observable<any> {
        return this.http.post(HOST + `/motivoIncidencia`, incidencia);
    }

    editar(incidencia: MotivoIncidenciaModel): Observable<any> {
        return this.http.put(
            HOST + `/motivoIncidencia/${incidencia.idMotivoIncidencia}`,
            incidencia
        );
    }

    eliminar(incidencia: MotivoIncidenciaModel): Observable<any> {
        return this.http.put(
            HOST + `/motivoIncidencia/eliminar/${incidencia.idMotivoIncidencia}`,
            incidencia
        );
    }

}
