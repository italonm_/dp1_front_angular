import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AgenciaModel, AgenciaNuevaModel } from "app/models/agencia.model";
import { Observable } from "rxjs";
import { HOST } from "app/shared/constant";

@Injectable({
    providedIn: "root",
})
export class AgenciaService {
    constructor(private http: HttpClient) {}

    listar(idBanco: number): Observable<any> {
        return this.http.get<AgenciaModel[]>(
            HOST + `/lugarcobro/banco/${idBanco}?size=1900`
        );
    }

    listarcajeros(idBanco: number): Observable<any> {
        return this.http.get<AgenciaModel[]>(
            HOST + `/lugarcobro/${idBanco}/cajeros`
        );
    }

    listarDepartamentos(): Observable<any> {
        return this.http.get<any>(HOST + `/distrito/departamento`);
    }

    listarProvincias(departamento: string): Observable<any> {
        return this.http.get<String>(
            HOST + `/distrito/provincia/${departamento}`
        );
    }

    listarDistritos(departamento: string, provincia: string): Observable<any> {
        return this.http.get<any>(
            HOST + `/distrito/lista/${provincia}/${departamento}`
        );
    }

    crear(agencia: AgenciaNuevaModel): Observable<any> {
        return this.http.post(HOST + `/lugarcobro`, agencia);
    }

    consultar(id: String): Observable<any> {
        return this.http.get<AgenciaNuevaModel>(HOST + `/lugarcobro/${id}`);
    }

    editar(agencia: AgenciaNuevaModel): Observable<any> {
        return this.http.put(
            HOST + `/lugarcobro/${agencia.idLugarCobro}`,
            agencia
        );
    }

    eliminar(agencia: AgenciaNuevaModel): Observable<any> {
        return this.http.put(
            HOST + `/lugarcobro/eliminar/${agencia.idLugarCobro}`,
            agencia
        );
    }

    crearMasivo(file: any, idBanco: string): Observable<any> {
        return this.http.post(HOST + `/csv/lugarCobro/${idBanco}`, file, {
            responseType: "blob",
        });
    }

    descargar(): Observable<any> {
        return this.http.post(HOST + `/csv/plantilla/4`, "zzz", {
            responseType: "blob",
        });
    }
}
