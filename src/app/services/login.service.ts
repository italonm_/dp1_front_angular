import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HOST } from "app/shared/constant";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class LoginService {
    constructor(private http: HttpClient) {}

    login(usuario: any): Observable<any> {
        return this.http.post(HOST + `/iniciarsesion`, usuario, {
            headers: new HttpHeaders()
                .set(
                    "Authorization",
                    "Basic " + btoa(usuario.username + ":" + usuario.password)
                )
                .set("Content-Type", "application/json"),
        });
    }

    changePassword(usuarioActual: any, usuarioNuevo: any): Observable<any>{
      return this.http.post(HOST + `/cambiarcontrasena`, usuarioNuevo, {
        headers: new HttpHeaders()
            .set(
                "Authorization",
                "Basic " + btoa(usuarioActual.username + ":" + usuarioActual.password)
            )
            .set("Content-Type", "application/json"),
      });
    }
}
