import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AgenciaModel } from "app/models/agencia.model";
import { HOST } from "app/shared/constant";
import { Observable, Subject } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class CalendarioService {
    constructor(private http: HttpClient) {}

    listar(): Observable<any> {
        const url =
            HOST +
            "/cronograma/lugarcobro/" +
            localStorage.getItem("cronograma") +
            "/" +
            localStorage.getItem("agencia");
        return this.http.get<any>(url);
    }

    consultar(id: number): Observable<any> {
        return this.http.get<any>(HOST + `/entidadBancaria/${id}`);
    }

    listarAgencias(idBanco: number): Observable<any> {
        return this.http.get<AgenciaModel[]>(
            HOST + `/lugarcobro/banco/${idBanco}` + "?size=2000"
        );
    }

    listarBeneficiarios(fecha: string): Observable<any> {
        const url =
            HOST +
            "/beneficiario/cronograma/" +
            localStorage.getItem("cronograma") +
            "/" +
            localStorage.getItem("agencia") +
            "/" +
            fecha;
        return this.http.get<AgenciaModel[]>(url);
    }
}
