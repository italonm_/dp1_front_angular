import { CronogramaModel } from 'app/models/cronograma.model';
import { AgenciaModel } from 'app/models/agencia.model';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HOST } from "app/shared/constant";
import { Observable } from "rxjs";
interface cronograma {
  idConfiguracion: number;
  nombre: string;
}

interface Horario{
  idCronograma: string;
  fechaIni: string;
  fechaFin: string;
  estado: string;
  lugarCobro: AgenciaModel;
  cronograma: CronogramaModel;
}

interface Cajero{
  lugarCobro: string;
  codigoHogar: number;
}

@Injectable({
    providedIn: "root",
})
export class ConsultaService {
    constructor(private http: HttpClient) {}

    consultar(codigo: number): Observable<any> {
        return this.http.get<Horario[]>(HOST + `/cronograma/beneficiario/${codigo}`)
    }

    listarCronogramas(): Observable<any> {
      return this.http.get<cronograma[]>(HOST + `/configuracion/listar/consulta`);
    }

    //cajero
    cobrar(cajero: Cajero): Observable<any> {
      return this.http.put(HOST + `/cronograma/cobrar/cajero`, cajero);
  }
}
