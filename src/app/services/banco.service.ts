import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BancoModel } from "app/models/banco.model";
import { HOST } from "app/shared/constant";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class BancoService {
    constructor(private http: HttpClient) {}

    listar(): Observable<any> {
        return this.http.get<BancoModel[]>(HOST + `/entidadBancaria`);
    }

    consultar(id: number): Observable<any> {
        return this.http.get<BancoModel>(HOST + `/entidadBancaria/${id}`);
    }

    crear(banco: BancoModel): Observable<any> {
        return this.http.post(HOST + `/entidadBancaria`, banco);
    }

    editar(banco: BancoModel): Observable<any> {
        return this.http.put(
            HOST + `/entidadBancaria/${banco.idEntidadBancaria}`,
            banco
        );
    }

    eliminar(banco: BancoModel): Observable<any> {
        return this.http.put(
            HOST + `/entidadBancaria/eliminar/${banco.idEntidadBancaria}`,
            banco
        );
    }
}
