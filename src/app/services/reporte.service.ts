import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ReporteModel } from "app/models/reporte.model";

import { HOST } from "app/shared/constant";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class ReporteService {
    constructor(private http: HttpClient) {}

    reporte(idConfiguracion: number): Observable<any> {
        return this.http.get<ReporteModel[]>(
            HOST + `/cronograma/reporte3/${idConfiguracion}`
        );
    }
}
