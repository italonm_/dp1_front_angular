import { AgenciaModel } from 'app/models/agencia.model';
import { BancoModel } from 'app/models/banco.model';


export class EmpleadoModel {
  idUsuario: string;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string; 
  telefono: string;
  correo: string;
  lugarCobro: AgenciaModel;
  entidadBancaria: BancoModel;
  estado: string;
}