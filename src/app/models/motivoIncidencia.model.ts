export class MotivoIncidenciaModel {
    idMotivoIncidencia: number;
    nombre: string;
    tipo: string;
}
