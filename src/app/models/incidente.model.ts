export class IncidenteModel {
    idIncidencia: number;
    idLugarCobro: string;
    codigoHogar: number;
    idMotivoIncidencia: number;
    descripcion: string;
    idConfiguracion: number;
    fecha: Date;
}
