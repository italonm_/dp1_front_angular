export class BancoModel {
    idEntidadBancaria: number;
    ruc: string;
    nombre: string;
    estado: string;
}
