export class CronogramaModel {
    idConfiguracion: number;
    nombre: string;
    porcentajeVarones: number;
    horarioPreferencial: string[];
    numMaxIncBeneficiario: number;
    numMaxIncLugarCobro: number;
    horariosPorBeneficiario: number;
    cantHorariosPenalizados: number;
    porcMaxContagio: number;
    porcVacantesPenalizado: number;
    fechaInicio: Date;
    reducPorcVacantes: number;
}
