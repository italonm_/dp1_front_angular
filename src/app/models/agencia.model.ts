import { Time } from '@angular/common';
import { DistritoModel } from './distrito.model';

export class AgenciaModel {
    idEntidadBancaria: number;
    idLugarCobro: string;
    codigo: string;
    nombre: string;
    departamento: string;
    provincia: string;
    distrito: string;
    direccion: string;
    horarioLV: string;
    horarioS: string;
    ncajas: number;
    atencionesHora: number;
    cantIncidencias: number;
    estado: string;
}
export class AgenciaNuevaModel {
    idEntidadBancaria: number;
    idLugarCobro: string;
    codigo: string;
    nombre: string;
    departamento: string;
    provincia: string;
    idDistrito: string;
    distrito: DistritoModel;
    direccion: string;
    horaAperturaLV: string;
    horaCierreLV: string;
    horaAperturaS: string;
    horaCierreS: string;
    tipo: string;
    numCajasAtencion: number;
    atencionesPorHora: number;
    cantIncidencias: number;
    estado: string;
}
