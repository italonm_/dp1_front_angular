export class wontFixModel {
    count:{
        TW:number;
    };
    label:string;
    chart: {
        TW:nameModel[];
    };
}

export class nameModel {
    name:string;
}

export class mainChartModel {
    series:seriesModel[];
    name:string;
}
export class seriesModel {
    name:string;
    value:number;
}

export class ReporteModel {
    widget5: {
        ranges:{
            TW:string;
        };
        supporting:{
            wontFix:wontFixModel;
            created: wontFixModel;
            reOpened: wontFixModel;
            closed: wontFixModel;
            fixed: wontFixModel;
            needsTest: wontFixModel;
        };
        title:string;
        mainChart:{
            TW: mainChartModel[];
        }
    };
    
}

export class Reporte2Model {
    Discapacitado:number;
    NoDiscapacitado:number;
    Cobrado:number;
    NoCobrado:number;
    Total:number;
    Masculino:number;
    Departamentos:Rep2Model;
    Femenino:number;

    
}
export class Rep2Model {
    Discapacitado:number;
    NoDiscapacitado:number;
    Total:number;
    
    Departamentos:string;
    

    
}