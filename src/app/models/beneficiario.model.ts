import { DistritoModel } from './distrito.model';


export class BeneficiarioModel {
    idBeneficiario: number;
    flagMayorEdad: number;
    flagDiscapacitado:number;
    genero: string;
    codigoHogar: number;
    departamento: string;
    provincia: string;
    distrito: string;
}

export class BeneficiarioModelAux {
    idBeneficiario: number;
    flagMayorEdad: number;
    flagDiscapacitado:number;
    genero: string;
    codigoHogar: number;
    distrito: {
        departamento:string;
        provincia:string;
        nombre:string;

    };
}

export class Beneficiario2Model {
    idBeneficiario: number;
    flagMayorEdad: number;
    flagDiscapacitado:number;
    genero: string;
    codigoHogar: number;
    cantidadIncidencias: number;
    estado: string;
    
}
export class BeneficiarioAgenciaModel {
    beneficiario: Beneficiario2Model;
    estado:string;
    idCronograma:string;
    fechaIni:Date;
    fechaFin:Date;
}
