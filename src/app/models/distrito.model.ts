export class DistritoModel {
    idDistrito: string;
    departamento: string;
    nombre: string;
    provincia: string;
    porcContagiados: any;
    ubigeo: string;
}
