import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FuseConfigService } from "@fuse/services/config.service";
import { fuseAnimations } from "@fuse/animations";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { FuseNavigationService } from "@fuse/components/navigation/navigation.service";
import { LoginService } from "app/services/login.service";
import { AuthEmpleadoGuard } from "app/services/auth-empleado.guard";
import { AuthAdminGuard } from "app/services/auth-admin.guard";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [MatSnackBar],
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    hide = true;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _matSnackBar: MatSnackBar,
        private loginService: LoginService,
        private router: Router,
        private _fuseNavigationService: FuseNavigationService,
        private authAdminService: AuthAdminGuard,
        private authEmpleadoService: AuthEmpleadoGuard
    ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: { hidden: true },
                toolbar: { hidden: true },
                footer: { hidden: true },
                sidepanel: { hidden: true },
            },
        };
    }

    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            username: ["", Validators.required],
            password: ["", Validators.required],
        });
    }

    login(): void {
        this.loginService.login(this.loginForm.value).subscribe(
            (data) => {
                localStorage.setItem(
                    "nombre",
                    data.usuario.nombre + " " + data.usuario.apellidoPaterno
                );
                localStorage.setItem("usuarioId", data.usuario.idUsuario);
                localStorage.setItem(
                    "username",
                    this.loginForm.value["username"]
                );
                if (data.usuario.rol === "Administrador") {
                    this.router.navigate([`admin/bancos`]);
                    this._fuseNavigationService.setCurrentNavigation("admin");

                    this.authAdminService.login();
                    console.log(this.authAdminService.status());
                } else if (data.usuario.rol === "Empleado") {
                    this.router.navigate([`empleado/beneficiarios`]);
                    this._fuseNavigationService.setCurrentNavigation(
                        "empleado"
                    );
                    this.authEmpleadoService.login();
                    localStorage.setItem("perfil", "empleado");
                    localStorage.setItem(
                        "agenciaId",
                        data.usuario.lugarCobro.idLugarCobro
                    );
                    localStorage.setItem(
                        "agenciaNombre",
                        data.usuario.lugarCobro.nombre +
                            " - " +
                            data.usuario.lugarCobro.entidadBancaria.nombre
                    );
                }
            },
            (_err) => {
                this._matSnackBar.open(
                    "Ingrese correctamente, el usuario y contraseña",
                    "",
                    {
                        verticalPosition: "top",
                        duration: 5000,
                        panelClass: ["my-snack-bar"],
                    }
                );
            }
        );
    }
}
