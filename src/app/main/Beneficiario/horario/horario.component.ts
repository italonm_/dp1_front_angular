import { IncidenteEmpFormComponent } from './../../Empleado/incidentes-emp/incidente-emp-form/incidente-emp-form.component';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { ConsultaService } from 'app/services/consulta.service';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'app-horario',
  templateUrl: './horario.component.html',
  styleUrls: ['./horario.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None,
})
export class HorarioComponent implements OnInit {
  public dataSource = new MatTableDataSource<any>();
  codigo:number;
  cronogramas = [];

  dialogRef: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator)
  set paginator(v: MatPaginator) {
      this.dataSource.paginator = v;
  }
  columnNames = [
      "cronograma",
      "lugarCobro",
      "fechaIni",
      "fechaFin",
      "estado",
      "registrarQueja"
  ];
  
  constructor(
    private consultaService: ConsultaService,
    private router: Router,
    private _matDialog: MatDialog,
    private _matSnackBar: MatSnackBar,
    private _fuseConfigService: FuseConfigService
  ) { 
    this._fuseConfigService.config = {
      layout: {
          navbar: { hidden: true },
          toolbar: { hidden: true },
          sidepanel: { hidden: true },
      },
    };
  }


  groupBy(objectArray): Array<Object> {
    return objectArray.reduce((acc, obj) => {
       const key = obj.configuracionCronograma.idConfiguracion;
       if (!acc[key]) {
          acc[key] = [];
       }
       // Add object to list for given key's value
       acc[key].push(obj);
       return acc;
    }, {});
 }

  ngOnInit(): void {
    this.codigo = parseInt(localStorage.getItem("codigoBeneficiario"));
    this.consultaService.consultar(this.codigo).subscribe(res => {
      const groupByCrono = this.groupBy(res)
      this.cronogramas = Object.entries(groupByCrono);
    });
  }

  registrarQueja(horario): void{
    this.dialogRef = this._matDialog.open(IncidenteEmpFormComponent, {
      panelClass: "incidente-emp-form-dialog",
      data: {
          action: "nuevaIncBenef",
          idLugarCobro: horario.lugarCobro.idLugarCobro
      },
    });
    this.dialogRef.afterClosed().subscribe((result) => {
        this.ngOnInit();
    });
  }

  logoutBenef():void{
    localStorage.removeItem("codigoBeneficiario");
    this.router.navigate([`beneficiario/consultar`]);
  }
}
