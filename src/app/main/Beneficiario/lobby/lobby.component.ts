import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { FuseConfigService } from "@fuse/services/config.service";
import { fuseAnimations } from "@fuse/animations";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FuseNavigationService } from "@fuse/components/navigation/navigation.service";


@Component({
    selector: "app-lobby",
    templateUrl: "./lobby.component.html",
    styleUrls: ["./lobby.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [MatSnackBar],
})
export class LobbyComponent implements OnInit {
    constructor(
        private router: Router,
        private _fuseConfigService: FuseConfigService,
        private _matSnackBar: MatSnackBar,
        private _fuseNavigationService: FuseNavigationService
    ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: { hidden: true },
                toolbar: { hidden: true },
                footer: { hidden: true },
                sidepanel: { hidden: true },
            },
        };
    }

    ngOnInit(): void {}
    login(): void {
        this.router.navigate([`auth`]);
    }
    consultar(): void {
        this.router.navigate([`beneficiario/consultar`]);
    }
    cajero(): void {
        this.router.navigate([`cajero`]);
    }
}
