import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FuseConfigService } from "@fuse/services/config.service";
import { fuseAnimations } from "@fuse/animations";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FuseNavigationService } from "@fuse/components/navigation/navigation.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ConsultaService } from "app/services/consulta.service";
import { Router } from "@angular/router";
interface Cronograma {
    idConfiguracion: number;
    nombre: string;
}

@Component({
    selector: "app-consulta",
    templateUrl: "./consulta.component.html",
    styleUrls: ["./consulta.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [MatSnackBar],
})
export class ConsultaComponent implements OnInit {
    form: FormGroup;
    hide = true;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _matSnackBar: MatSnackBar,
        private consultaService: ConsultaService,
        private router: Router,
        private _fuseNavigationService: FuseNavigationService
    ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: { hidden: true },
                toolbar: { hidden: true },
                footer: { hidden: true },
                sidepanel: { hidden: true },
            },
        };
        this.form = this.consultaForm();
    }

    ngOnInit(): void {}

    consultaForm(): FormGroup {
        return this._formBuilder.group({
            codigo: [, Validators.required],
        });
    }

    consultar(): void {
        let code = this.form.value.codigo;
        this.consultaService.consultar(code).subscribe(
            (data) => {
                this.router.navigate([`beneficiario/horario`]);
                //this._fuseNavigationService.setCurrentNavigation("admin");
                localStorage.setItem("codigoBeneficiario", code);
            },
            (_err) => {
                this._matSnackBar.open("Código inválido", "", {
                    verticalPosition: "top",
                    duration: 5000,
                    panelClass: ["my-snack-bar"],
                });
            }
        );
    }
}
