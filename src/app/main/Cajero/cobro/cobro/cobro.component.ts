import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { fuseAnimations } from "@fuse/animations";
import { FuseNavigationService } from "@fuse/components/navigation/navigation.service";
import { FuseConfigService } from "@fuse/services/config.service";
import { ConsultaService } from "app/services/consulta.service";

@Component({
    selector: "app-cobro",
    templateUrl: "./cobro.component.html",
    styleUrls: ["./cobro.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [MatSnackBar],
})
export class CobroComponent implements OnInit {
    form: FormGroup;
    hide = true;
    cajeronombre: string;
    idlugarcobro: string;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _matSnackBar: MatSnackBar,
        private consultaService: ConsultaService,
        private router: Router,
        private _fuseNavigationService: FuseNavigationService
    ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: { hidden: true },
                toolbar: { hidden: true },
                footer: { hidden: true },
                sidepanel: { hidden: true },
            },
        };
        this.form = this.consultaForm();
    }

    ngOnInit(): void {
        this.cajeronombre = localStorage.getItem("cajeronombre");
        this.idlugarcobro = localStorage.getItem("idcajero");
    }

    consultaForm(): FormGroup {
        return this._formBuilder.group({
            codigo: [, Validators.required],
            agencia: [""],
        });
    }

    cobrar(): void {
        let data = {
            codigoHogar: this.form.value.codigo,
            lugarCobro: this.idlugarcobro,
        };
        console.log(JSON.stringify(data, null, "\t"));
        this.consultaService.cobrar(data).subscribe(
            (data) => {
                this._matSnackBar.open(data.mensaje, "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
            },
            (_err) => {
                this._matSnackBar.open("Código inválido", "", {
                    verticalPosition: "top",
                    duration: 5000,
                    panelClass: ["my-snack-bar"],
                });
            }
        );
    }
}
