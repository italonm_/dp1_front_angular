import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseConfigService } from '@fuse/services/config.service';
import { AgenciaModel } from 'app/models/agencia.model';
import { BancoModel } from 'app/models/banco.model';
import { AgenciaService } from 'app/services/agencia.service';
import { BancoService } from 'app/services/banco.service';
import { Observable } from 'rxjs';
import { map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-cajero',
  templateUrl: './cajero.component.html',
  styleUrls: ['./cajero.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations,
  providers: [MatSnackBar],
})
export class CajeroComponent implements OnInit {

  form: FormGroup;
  bancos: BancoModel[] = [];
  agencias: AgenciaModel[] = [];
  agenciaNombre: string;
  idcajero: string;
  filteredOptions: Observable<any[]>;
  hide = true;

    constructor(
      private _fuseConfigService: FuseConfigService,
      private _formBuilder: FormBuilder,
      private _matSnackBar: MatSnackBar,
      private bancoService: BancoService,
      private agenciaService: AgenciaService,
      private router: Router,
      private _fuseNavigationService: FuseNavigationService
      ) {
        this._fuseConfigService.config = {
            layout: {
                navbar: { hidden: true },
                toolbar: { hidden: true },
                footer: { hidden: true },
                sidepanel: { hidden: true },
            },
        };
        this.form = this.cajeroForm();
        
        this.bancoService.listar().subscribe((res) => {
          this.bancos = res;
        });
    }

    onChangeBanco(banco: any){
      let bancoId = banco.idEntidadBancaria;
      this.listarAgencias(bancoId)
    }
  
    listarAgencias(bancoId: number){
      if (this.form.value.agencia) this.form.patchValue( {'agencia':""} );
      this.agencias = [];
      this.agenciaService.listarcajeros(bancoId).subscribe((res) => {
        this.agencias = res;
      });
      this.filteredOptions = this.form.get("agencia").valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.nombre),
        map(name => name ? this._filter(name) : this.agencias.slice())
      );
      
    }

    private _filter(value: string): any[] {
      if (value){
        const filterValue = value.toLowerCase();
        this.agenciaNombre= value.toLowerCase();
        return this.agencias.filter(option => option.nombre.toLowerCase().includes(filterValue));
      }
    }
  
    compareWith(banco1: any, banco2: any): boolean {
      return banco1 && banco2 && banco1.idEntidadBancaria === banco2.idEntidadBancaria;
    }

    onSelectConvertValueToInputFn(agencia): string{
      this.idcajero=agencia.idLugarCobro;
      console.log("id",this.idcajero, agencia);
      localStorage.setItem("idcajero",this.idcajero)
      return agencia ? agencia.nombre : agencia;
    }

    ngOnInit(): void {
    }

    cajeroForm(): FormGroup {
      return this._formBuilder.group({
        banco: ['',Validators.required],
        agencia: ['',[Validators.required,this.RequireMatch]],
      });
    }

    RequireMatch(control: AbstractControl) {
      const selection: any = control.value;
      if (typeof selection === 'string') {
          return { incorrect: true };
      }
      return null;
  }

    consultar():void{
      //let code = this.form.value.codigo;
      this.router.navigate([`cajero/cobro`]);
      localStorage.setItem("cajeronombre",this.agenciaNombre);
    }

}
