import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-banco-autocomplete',
  templateUrl: './banco-autocomplete.component.html',
  styleUrls: ['./banco-autocomplete.component.scss']
})
export class BancoAutocompleteComponent implements OnInit {
  myControl = new FormControl();
  options: Array<{ id: number; name: string }> = [
    { id: 5, name: 'One'}, 
    { id: 6, name: 'Two'}, 
    { id: 7, name: 'Three'}
  ];
  filteredOptions: Observable<object[]>;

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): object[] {
    const filterValue = value.toLowerCase();
    let selectedOption = this.options.filter(option => option.name.toLowerCase().includes(filterValue));
    return selectedOption;
  }

  /*displayFn(object: User): string {
    return user ? user.name : user;
  }*/

  displayFn(obj: {id: number; name: string}): string {
    return obj.name;
  }

  returnFn(obj: {id: number; name: string}): number | undefined {
    return obj.id;
  }

}
