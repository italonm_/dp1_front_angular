import { AgenciaService } from './../../../../services/agencia.service';
import { AgenciaModel } from 'app/models/agencia.model';
import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { BancoModel } from 'app/models/banco.model';
import { EmpleadoModel } from 'app/models/empleado.model';
import { BancoService } from 'app/services/banco.service';
import { EmpleadoService } from 'app/services/empleado.service';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ActivationEnd } from '@angular/router';

interface Actividad {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-empleado-form',
  templateUrl: './empleado-form.component.html',
  styleUrls: ['./empleado-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MatSnackBar, MatSnackBarConfig]
})
export class EmpleadoFormComponent implements OnInit {
  form: FormGroup;
  dialogTitle: string;
  empleado = new EmpleadoModel();
  bancos: BancoModel[] = [];
  agencias: AgenciaModel[] = [];
  filteredOptions: Observable<any[]>;
  estados: Actividad[] = [
    { value: "activo", viewValue: "Activo" },
    { value: "Inactivo", viewValue: "Inactivo" },
  ];

  constructor(
    public matDialogRef: MatDialogRef<EmpleadoFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _matSnackBar: MatSnackBar,
    private empleadoService: EmpleadoService,
    private _formBuilder: FormBuilder,
    private bancoService: BancoService,
    private agenciaService: AgenciaService

  ) { 
    if (data.action === "editar") {
      this.dialogTitle = "Editar Empleado";
      this.empleadoService.consultar(data.id).subscribe(
          (res) => {
              this.empleado = res;
              this.form = this.empleadoForm();
          },
          (err) => {
              this._matSnackBar.open("Error de consulta", "ok", {
                  verticalPosition: "bottom",
                  panelClass: ["my-snack-bar"],
              });
          }
      );
    } else {
        this.dialogTitle = "Nuevo Empleado";
    }
    this.form = this.empleadoForm();
    this.bancoService.listar().subscribe((res) => {
      this.bancos = res;
    });
  }

  onChangeBanco(banco: any){
    let bancoId = banco.idEntidadBancaria;
    this.listarAgencias(bancoId)
  }

  listarAgencias(bancoId: number){
    if (this.form.value.agencia) this.form.patchValue( {'agencia':""} );
    this.agencias = [];
    this.empleadoService.listarAgenciasPorBanco(bancoId).subscribe((res) => {
      this.agencias = res;
    });
    this.filteredOptions = this.form.get("agencia").valueChanges
    .pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombre),
      map(name => name ? this._filter(name) : this.agencias.slice())
    );
  }

  ngOnInit(): void {
  }

  private _filter(value: string): any[] {
    if (value){
      const filterValue = value.toLowerCase();
      return this.agencias.filter(option => option.nombre.toLowerCase().includes(filterValue));
    }
  }

  compareWith(banco1: any, banco2: any): boolean {
    return banco1 && banco2 && banco1.idEntidadBancaria === banco2.idEntidadBancaria;
  }

  empleadoForm(): FormGroup {
    return this._formBuilder.group({
      idUsuario: [this.empleado.idUsuario],
      nombre: [this.empleado.nombre, [
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.maxLength(150)
      ]], 
      apellidoPaterno: [this.empleado.apellidoPaterno, [
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.maxLength(150)
      ]],
      apellidoMaterno: [this.empleado.apellidoMaterno, [
        Validators.pattern("^[a-zA-Z]+$"),
        Validators.maxLength(150)
      ]],
      telefono: [this.empleado.telefono,
        [
          Validators.pattern("^[0-9]*$"),
          Validators.minLength(6),
          Validators.maxLength(9),
        ],
      ],
      correo: [this.empleado.correo,
          Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,4}$")
      ],
      banco: [this.empleado.entidadBancaria],
      agencia: [this.empleado.lugarCobro],
      estado: [this.empleado.estado],
    });
  }

  crearEmpleado(): void{
    this.form.patchValue({
      estado: "Activo",
    });
    let formData = this.form.value;
    let data = {
      username: formData.correo,
      contrasena: "prueba",
      usuario: {
        lugarCobro: {
          idLugarCobro: formData.agencia.idLugarCobro
        },
        nombre: formData.nombre,
        apellidoPaterno: formData.apellidoPaterno,
        apellidoMaterno: formData.apellidoMaterno,
        telefono: formData.telefono,
        correo: formData.correo,
        rol: "Empleado",
        estado: formData.estado
      }
    }
    
    console.log(JSON.stringify(data,null,'\t'));
    this.empleadoService.crear(data).subscribe(
      (res) => {
          this._matSnackBar.open("Grabación exitosa", "", {
              verticalPosition: "top",
              duration: 5000,
          });
          this.matDialogRef.close();
      },
      (err) => {
        if (err.error.mensaje){
          this._matSnackBar.open("Error, "+err.error.mensaje, "", {
            verticalPosition: "top",
            duration: 5000,
          });
        } else {
          this._matSnackBar.open("Error, empleado no registrado", "", {
            verticalPosition: "top",
            duration: 5000,
          });
        }
      }
    );
  }

  editarEmpleado(): void {
    let formData = this.form.value;
    let data = {
        lugarCobro: {
          idLugarCobro: formData.agencia.idLugarCobro
        },
        nombre: formData.nombre,
        apellidoPaterno: formData.apellidoPaterno,
        apellidoMaterno: formData.apellidoMaterno,
        telefono: formData.telefono,
        correo: formData.correo,
        rol: "Empleado",
        estado: formData.estado
    }
    this.empleadoService.editar(formData.idUsuario, data).subscribe(
        (res) => {
            this._matSnackBar.open("Grabación exitosa", "", {
                verticalPosition: "top",
                duration: 5000,
            });
            this.matDialogRef.close();
        },
        (err) => {
          if (err.error.mensaje){
            this._matSnackBar.open("Error, "+err.error.mensaje, "", {
              verticalPosition: "top",
              duration: 5000,
            });
          } else {
            this._matSnackBar.open("Error, empleado no actualizado", "", {
              verticalPosition: "top",
              duration: 5000,
            });
          }
        }
    );
  }

  onSelectConvertValueToInputFn(agencia): string{
    return agencia ? agencia.nombre : agencia;
  }

}
