import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { fuseAnimations } from "@fuse/animations";
import { EmpleadoService } from "../../../services/empleado.service";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { EmpleadoFormComponent } from "./empleado-form/empleado-form.component";
import { MessageBoxComponent } from "app/shared/message-box/message-box.component";
import { EmpleadoModel } from "app/models/empleado.model";
import { MatSnackBar } from "@angular/material/snack-bar";
import { EmpleadoMasivoComponent } from "./empleado-masivo/empleado-masivo.component";

@Component({
    selector: "app-empleados",
    templateUrl: "./empleados.component.html",
    styleUrls: ["./empleados.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class EmpleadosComponent implements OnInit {
    public dataSource = new MatTableDataSource<any>();
    visible: boolean;
    dialogRef: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator)
    set paginator(v: MatPaginator) {
        this.dataSource.paginator = v;
    }
    columnNames = [
        //"idUsuario",
        "nombre",
        "telefono",
        "correo",
        "entidadBancaria",
        "lugarCobro",
        "estado",
        "editar",
        "eliminar",
    ];

    constructor(
        private empleadoService: EmpleadoService,
        private router: Router,
        private _matDialog: MatDialog,
        private _matSnackBar: MatSnackBar
    ) {}

    ngOnInit(): void {
        this.visible = false;
        this.empleadoService.listar().subscribe((res) => {
            this.visible = true;
            this.dataSource.data = res.content;
            this.dataSource.sort = this.sort;
        });
    }

    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    };

    crear(): void {
        this.dialogRef = this._matDialog.open(EmpleadoFormComponent, {
            panelClass: "empleado-form-dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe(() => {
            this.ngOnInit();
        });
    }

    cargar(): void {
        this.dialogRef = this._matDialog.open(EmpleadoMasivoComponent, {
            panelClass: "empleado-masivo-dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe(() => {
            this.ngOnInit();
        });
    }

    editar(id: number): void {
        this.dialogRef = this._matDialog.open(EmpleadoFormComponent, {
            panelClass: "empleado-form-dialog",
            data: {
                action: "editar",
                id: id,
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    eliminar(empleado: EmpleadoModel): void {
        this.dialogRef = this._matDialog.open(MessageBoxComponent, {
            panelClass: "confirm-form-dialog",
            data: {
                titulo: "Eliminar empleado: " + empleado.nombre,
                subTitulo: "¿Está seguro de eliminar el empleado?",
                botonAceptar: "Si",
                botonCancelar: "No",
            },
        });

        this.dialogRef.afterClosed().subscribe((result) => {
            if (localStorage.getItem("eliminar") === "S") {
                this.empleadoService.eliminar(empleado).subscribe(
                    (data) => {
                        this._matSnackBar.open("Se eliminó el empleado", "", {
                            verticalPosition: "top",
                            horizontalPosition: "center",
                            duration: 5000,
                        });
                        this.ngOnInit();
                    },
                    (err) => {
                        this._matSnackBar.open(
                            "Error, empleado no eliminado",
                            "",
                            {
                                verticalPosition: "top",
                                duration: 5000,
                            }
                        );
                    }
                );
            }
            localStorage.setItem("eliminar", "N");
        });
    }
}
