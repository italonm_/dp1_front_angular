import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { AgenciaModel } from "app/models/agencia.model";
import { BancoModel } from "app/models/banco.model";
import { BancoService } from "app/services/banco.service";
import { CalendarioService } from "app/services/calendario.service";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";

@Component({
    selector: "app-select-agencia-form",
    templateUrl: "./select-agencia-form.component.html",
    styleUrls: ["./select-agencia-form.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class SelectAgenciaFormComponent implements OnInit {
    banco = new FormControl("");
    bancos: BancoModel[] = [];
    agencia = new FormControl("");
    agencias: AgenciaModel[] = [];
    filteredOptions: Observable<any[]>;

    constructor(
        public matDialogRef: MatDialogRef<SelectAgenciaFormComponent>,
        private _matSnackBar: MatSnackBar,
        private bancoService: BancoService,
        private agenciaService: CalendarioService
    ) {}

    ngOnInit(): void {
        this.bancoService.listar().subscribe((res) => {
            this.bancos = res;
        });
        this.filteredOptions = this.agencia.valueChanges.pipe(
            startWith(""),
            map((value) => this._filter(value))
        );
    }

    onChangeBanco(bancoId: number): void {
        this.bancoService.consultar(bancoId).subscribe(
            (res) => {
                localStorage.setItem("banconombre", res.nombre);
            },
            (err) => {
                this._matSnackBar.open("Error de consulta", "ok", {
                    verticalPosition: "bottom",
                    panelClass: ["my-snack-bar"],
                });
            }
        );
        this.agencias = [];
        this.agenciaService.listarAgencias(bancoId).subscribe((res) => {
            this.agencias = res.content;
        });
    }
    private _filter(value: string): any[] {
        if (value) {
            const filterValue = value.toString().toLowerCase();
            return this.agencias.filter((option) =>
                option.nombre.toLowerCase().includes(filterValue)
            );
        }
    }
    onSelectConvertValueToInputFn(agencia): string {
        return agencia ? agencia.nombre : agencia;
    }

    buscarAgencia(): void {
        if (this.agencia.value.idLugarCobro) {
            this._matSnackBar.open(
                "Agencia " + this.agencia.value.nombre + " seleccionada",
                "",
                {
                    verticalPosition: "top",
                    duration: 5000,
                }
            );
            localStorage.setItem("agencia", this.agencia.value.idLugarCobro);
            localStorage.setItem("agencianombre", this.agencia.value.nombre);
            this.matDialogRef.close();
        } else {
            this._matSnackBar.open("Error, seleccione una agencia válida", "", {
                verticalPosition: "top",
                duration: 5000,
            });
        }
    }
}
