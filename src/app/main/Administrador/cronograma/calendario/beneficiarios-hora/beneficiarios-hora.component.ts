import { DatePipe } from "@angular/common";
import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { CalendarioService } from "app/services/calendario.service";

@Component({
    selector: "app-beneficiarios-hora",
    templateUrl: "./beneficiarios-hora.component.html",
    styleUrls: ["./beneficiarios-hora.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class BeneficiariosHoraComponent implements OnInit {
    entrega: any;
    beneficiarios: any;
    hombres = 0;
    mujeres = 0;
    infractores = 0;
    discapacitados = 0;
    constructor(
        public matDialogRef: MatDialogRef<BeneficiariosHoraComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _calendarioService: CalendarioService,
        public datepipe: DatePipe
    ) {}

    ngOnInit(): void {
        const hora = this.datepipe.transform(
            this._data.event.start,
            "yyyy-MM-dd HH:mm"
        );
        this._calendarioService.listarBeneficiarios(hora).subscribe((res) => {
            this.beneficiarios = res;
            this.beneficiarios.forEach((b) => {
                if (b.genero === "Masculino") {
                    this.hombres++;
                } else {
                    this.mujeres++;
                }
                if (b.flagDiscapacitado === 1) {
                    this.discapacitados++;
                }
                if (b.cantidadIncidencias > 0) {
                    this.infractores++;
                }
            });
        });
    }
}
