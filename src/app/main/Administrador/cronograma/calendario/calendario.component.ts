import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { fuseAnimations } from "@fuse/animations";
import { FuseConfirmDialogComponent } from "@fuse/components/confirm-dialog/confirm-dialog.component";
import {
    CalendarEvent,
    CalendarEventTimesChangedEvent,
    CalendarMonthViewDay,
} from "angular-calendar";
import { CalendarioService } from "app/services/calendario.service";
import { isSameDay, isSameMonth, startOfDay } from "date-fns";
import { Subject } from "rxjs";
import { BeneficiariosHoraComponent } from "./beneficiarios-hora/beneficiarios-hora.component";
import { SelectAgenciaFormComponent } from "./select-agencia-form/select-agencia-form.component";

@Component({
    selector: "app-calendario",
    templateUrl: "./calendario.component.html",
    styleUrls: ["./calendario.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class CalendarioComponent implements OnInit {
    activeDayIsOpen: boolean;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    dialogRef: any;
    events: CalendarEvent[];
    refresh: Subject<any> = new Subject();
    selectedDay: any;
    view: string;
    viewDate: Date;
    locale = "Es";
    banco: string;
    agencia: string;

    constructor(
        private _matDialog: MatDialog,
        private _calendarioService: CalendarioService
    ) {
        this.view = "week";
        this.viewDate = new Date();
        this.activeDayIsOpen = true;
        this.selectedDay = { date: startOfDay(new Date()) };
    }

    ngOnInit(): void {
        this.buscarAgencia();
    }

    verBeneficiarios(action: string, event: CalendarEvent): void {
        this.dialogRef = this._matDialog.open(BeneficiariosHoraComponent, {
            panelClass: "beneficiarios-hora-form-dialog",
            data: {
                event: event,
                action: action,
            },
        });
    }

    buscarAgencia(): void {
        this.dialogRef = this._matDialog.open(SelectAgenciaFormComponent, {
            panelClass: "select-agencia-form-dialog",
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.setEvents();
        });
    }

    setEvents(): void {
        this.banco = localStorage.getItem("banconombre");
        this.agencia = localStorage.getItem("agencianombre");
        localStorage.removeItem("banconombre");
        localStorage.removeItem("agencianombre");
        this._calendarioService.listar().subscribe((res) => {
            this.events = res;
            this.viewDate = new Date(this.events[0].start);
            this.events.forEach((event) => {
                event.start = new Date(event.start);
                event.end = new Date(event.end);
            });
        });
    }

    beforeMonthViewRender({ header, body }): void {
        const _selectedDay = body.find((_day) => {
            return _day.date.getTime() === this.selectedDay.date.getTime();
        });

        if (_selectedDay) {
            _selectedDay.cssClass = "cal-selected";
        }
    }

    dayClicked(day: CalendarMonthViewDay): void {
        const date: Date = day.date;
        const events: CalendarEvent[] = day.events;

        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) &&
                    this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
        this.selectedDay = day;
        this.refresh.next();
    }

    eventTimesChanged({
        event,
        newStart,
        newEnd,
    }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        this.refresh.next(true);
    }
}
