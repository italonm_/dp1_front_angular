import {
    ChangeDetectorRef,
    Component,
    Inject,
    OnInit,
    ViewEncapsulation,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { CronogramaModel } from "app/models/cronograma.model";
import { CronogramaService } from "app/services/cronograma.service";

@Component({
    selector: "app-configuracion-form",
    templateUrl: "./configuracion-form.component.html",
    styleUrls: ["./configuracion-form.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class ConfiguracionFormComponent implements OnInit {
    form: FormGroup;
    configuracion = new CronogramaModel();
    horas: string[] = [
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20",
        "21",
        "22",
        "23",
    ];
    constructor(
        public matDialogRef: MatDialogRef<ConfiguracionFormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar: MatSnackBar,
        private _cronogramaService: CronogramaService,
        private _formBuilder: FormBuilder,
        private cdRef: ChangeDetectorRef
    ) {
        this._cronogramaService.consultarConfiguracion().subscribe(
            (res) => {
                this.configuracion = res;
                this.form = this.configuracionForm();
            },
            (err) => {
                this._matSnackBar.open("Error de consulta", "ok", {
                    verticalPosition: "bottom",
                    panelClass: ["my-snack-bar"],
                });
            }
        );
        this.form = this.configuracionForm();
    }

    ngOnInit(): void {}

    ngAfterViewChecked(): void {
        this.cdRef.detectChanges();
    }

    configuracionForm(): FormGroup {
        return this._formBuilder.group({
            fechaInicio: [this.configuracion.fechaInicio],
            porcentajeVarones: [
                this.configuracion.porcentajeVarones,
                [Validators.pattern("\\d+\\.\\d{2}"), Validators.max(100)],
            ],
            horarioPreferencial: [this.configuracion.horarioPreferencial],
            numMaxIncBeneficiario: [this.configuracion.numMaxIncBeneficiario],
            numMaxIncLugarCobro: [this.configuracion.numMaxIncLugarCobro],
            horariosPorBeneficiario: [
                this.configuracion.horariosPorBeneficiario,
            ],
            cantHorariosPenalizados: [
                this.configuracion.cantHorariosPenalizados,
            ],
            porcMaxContagio: [
                this.configuracion.porcMaxContagio,
                [Validators.pattern("\\d+\\.\\d{2}"), Validators.max(100)],
            ],
            porcVacantesPenalizado: [
                this.configuracion.porcVacantesPenalizado,
                [Validators.pattern("\\d+\\.\\d{2}"), Validators.max(100)],
            ],
            reducPorcVacantes: [
                this.configuracion.reducPorcVacantes,
                [Validators.pattern("\\d+\\.\\d{2}"), Validators.max(100)],
            ],
        });
    }

    actualizarConfiguracion(): void {
        this._cronogramaService
            .actualizarConfiguracion(this.form.value)
            .subscribe(
                (res) => {
                    this._matSnackBar.open("Grabación exitosa", "", {
                        verticalPosition: "top",
                        duration: 5000,
                    });
                    this.matDialogRef.close();
                },
                (err) => {
                    this._matSnackBar.open("Error de consulta", "ok", {
                        verticalPosition: "bottom",
                        panelClass: ["my-snack-bar"],
                    });
                }
            );
    }

    porcentajeVarones(): boolean {
        return this.form.get("porcentajeVarones").invalid;
    }

    reducPorcVacantes(): boolean {
        return this.form.get("reducPorcVacantes").invalid;
    }

    porcMaxContagio(): boolean {
        return this.form.get("porcMaxContagio").invalid;
    }

    porcVacantesPenalizado(): boolean {
        return this.form.get("porcVacantesPenalizado").invalid;
    }
}
