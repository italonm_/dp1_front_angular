import {
    AfterViewChecked,
    ChangeDetectorRef,
    Component,
    Inject,
    OnInit,
    ViewEncapsulation,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { CronogramaModel } from "app/models/cronograma.model";
import { CronogramaService } from "app/services/cronograma.service";

@Component({
    selector: "app-cronograma-form",
    templateUrl: "./cronograma-form.component.html",
    styleUrls: ["./cronograma-form.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class CronogramaFormComponent implements OnInit, AfterViewChecked {
    form: FormGroup;
    visible: boolean;
    dialogTitle: string;
    cronograma = new CronogramaModel();
    horas: string[] = [
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20",
        "21",
        "22",
        "23",
    ];

    constructor(
        public matDialogRef: MatDialogRef<CronogramaFormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar: MatSnackBar,
        private _cronogramaService: CronogramaService,
        private _formBuilder: FormBuilder,
        private cdRef: ChangeDetectorRef
    ) {
        if (data.action === "ver") {
            this._cronogramaService.consultar(data.id).subscribe(
                (res) => {
                    this.dialogTitle = res.nombre;
                    this.cronograma = res;
                    this.form = this.cronogramaForm();
                },
                (err) => {
                    this._matSnackBar.open("Error de consulta", "ok", {
                        verticalPosition: "bottom",
                        panelClass: ["my-snack-bar"],
                    });
                }
            );
        } else {
            this.dialogTitle = "Nuevo cronograma";
        }
        this.form = this.cronogramaForm();
    }

    ngOnInit(): void {}

    ngAfterViewChecked(): void {
        this.cdRef.detectChanges();
    }

    cronogramaForm(): FormGroup {
        return this._formBuilder.group({
            nombre: [this.cronograma.nombre],
            fechaInicio: [this.cronograma.fechaInicio],
            porcentajeVarones: [
                this.cronograma.porcentajeVarones,
                [Validators.pattern("\\d+\\.\\d{2}"), Validators.max(100)],
            ],
            horarioPreferencial: [this.cronograma.horarioPreferencial],
            numMaxIncBeneficiario: [this.cronograma.numMaxIncBeneficiario],
            numMaxIncLugarCobro: [this.cronograma.numMaxIncLugarCobro],
            horariosPorBeneficiario: [this.cronograma.horariosPorBeneficiario],
            cantHorariosPenalizados: [this.cronograma.cantHorariosPenalizados],
            porcMaxContagio: [
                this.cronograma.porcMaxContagio,
                [Validators.pattern("\\d+\\.\\d{2}"), Validators.max(100)],
            ],
            porcVacantesPenalizado: [
                this.cronograma.porcVacantesPenalizado,
                [Validators.pattern("\\d+\\.\\d{2}"), Validators.max(100)],
            ],
            reducPorcVacantes: [
                this.cronograma.reducPorcVacantes,
                [Validators.pattern("\\d+\\.\\d{2}"), Validators.max(100)],
            ],
        });
    }

    crearCronograma(): void {
        const today = new Date();
        var date: Date = new Date();
        date.setHours(0, 0, 0, 0);
        // console.log(date.getTime());
        // console.log(new Date(this.form.value.fechaInicio).getTime());
        // console.log(
        //     date.getTime() - new Date(this.form.value.fechaInicio).getTime()
        // );
        if (date.getTime() > new Date(this.form.value.fechaInicio).getTime()) {
            this._matSnackBar.open("Error, seleccione una fecha futura", "", {
                verticalPosition: "top",
                horizontalPosition: "center",
                duration: 5000,
            });
            return;
        }
        if (new Date(this.form.value.fechaInicio).getDay() == 6) {
            this._matSnackBar.open(
                "Error, el cronograma no puede iniciar un domingo",
                "",
                {
                    verticalPosition: "top",
                    horizontalPosition: "center",
                    duration: 5000,
                }
            );
            return;
        }
        this.visible = false;
        this._cronogramaService.crear(this.form.value).subscribe(
            (data) => {
                console.log(data);
                if (data.type === "application/csv") {
                    const a = document.createElement("a");
                    const objectUrl = URL.createObjectURL(data);
                    a.href = objectUrl;
                    a.download = "errores.csv";
                    a.click();
                    URL.revokeObjectURL(objectUrl);
                    this._matSnackBar.open(
                        "Cronograma generado, existen ubigeos sin lugares de cobro",
                        "",
                        {
                            verticalPosition: "top",
                            horizontalPosition: "center",
                            duration: 5000,
                        }
                    );
                } else {
                    this._matSnackBar.open(
                        "Cronograma generado exitosamente",
                        "",
                        {
                            verticalPosition: "top",
                            horizontalPosition: "center",
                            duration: 5000,
                        }
                    );
                }
                this.visible = true;
                this.matDialogRef.close();
            },
            (err) => {
                console.log(err);
                this._matSnackBar.open(
                    "Fecha ocupada por otro cronograma",
                    "",
                    {
                        verticalPosition: "top",
                        duration: 5000,
                    }
                );
                this.visible = true;
            }
        );
    }

    configuracionActual(): void {
        this._cronogramaService.consultarConfiguracion().subscribe(
            (res) => {
                this.cronograma = res;
                this.form = this.cronogramaForm();
            },
            (err) => {
                this._matSnackBar.open("Error de consulta", "ok", {
                    verticalPosition: "bottom",
                    panelClass: ["my-snack-bar"],
                });
            }
        );
    }

    porcentajeVarones(): boolean {
        return this.form.get("porcentajeVarones").invalid;
    }

    reducPorcVacantes(): boolean {
        return this.form.get("reducPorcVacantes").invalid;
    }

    porcMaxContagio(): boolean {
        return this.form.get("porcMaxContagio").invalid;
    }

    porcVacantesPenalizado(): boolean {
        return this.form.get("porcVacantesPenalizado").invalid;
    }
}
