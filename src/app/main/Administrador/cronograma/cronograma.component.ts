import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { fuseAnimations } from "@fuse/animations";
import { CronogramaService } from "app/services/cronograma.service";
import { ConfiguracionFormComponent } from "./configuracion-form/configuracion-form.component";
import { CronogramaFormComponent } from "./cronograma-form/cronograma-form.component";

@Component({
    selector: "app-cronograma",
    templateUrl: "./cronograma.component.html",
    styleUrls: ["./cronograma.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class CronogramaComponent implements OnInit {
    dialogRef: any;
    viewDate: Date;
    cronogramas = [];
    constructor(
        private _matDialog: MatDialog,
        private router: Router,
        private _cronogramaService: CronogramaService
    ) {}

    ngOnInit(): void {
        this.viewDate = new Date();
        this._cronogramaService.listar().subscribe((res) => {
            this.cronogramas = res;
        });
    }

    crear(): void {
        this.dialogRef = this._matDialog.open(CronogramaFormComponent, {
            panelClass: "cronograma-form-dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    actualizarConfiguracion(): void {
        this.dialogRef = this._matDialog.open(ConfiguracionFormComponent, {
            panelClass: "configuracion-form-dialog",
            data: {
                action: "configuracion",
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    verCalendario(id: number): void {
        this.router.navigate([`admin/calendario`]);
        localStorage.setItem("cronograma", id.toString());
    }

    verConfiguracion(id: number): void {
        this.dialogRef = this._matDialog.open(CronogramaFormComponent, {
            panelClass: "cronograma-form-dialog",
            data: {
                action: "ver",
                id: id,
            },
        });
    }
}
