import { ReporteService } from "./../../../services/reporte.service";
import { Component, OnInit, ViewEncapsulation, ViewChild } from "@angular/core";
import * as shape from "d3-shape";
import { CronogramaService } from "app/services/cronograma.service";
import { fuseAnimations } from "@fuse/animations";

@Component({
    selector: "app-reportes",
    templateUrl: "./reportes.component.html",
    styleUrls: ["./reportes.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class ReportesComponent implements OnInit {
    initialized: boolean;
    loading: boolean;
    cronogramas = [];

    estiloRecogidos: any;
    estiloAgenciaDepartamento: any;
    estiloAgenciasInfractoras: any;
    estiloDepartamentosInfractores: any;
    estiloAtencionDepartamento: any;

    reporteRecogidos: any;
    reporteAgenciaDepartamento: any;
    reporteAgenciasInfractoras: any;
    reporteDepartamentosInfractores: any;
    reporteAtencionDepartamento: any;
    reporteLabelAtencion: any;

    totalAgencias: any;
    agenciasActivas: any;
    constructor(
        private reporteService: ReporteService,
        private _cronogramaService: CronogramaService
    ) {
        this.initialized = false;
        this.estiloRecogidos = {
            currentRange: "TW",
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: "Days",
            showYAxisLabel: false,
            yAxisLabel: "Isues",
            scheme: {
                domain: ["#42BFF7", "#C6ECFD", "#C7B42C", "#AAAAAA"],
            },
            onSelect: (ev) => {
                console.log(ev);
            },
            supporting: {
                currentRange: "",
                xAxis: false,
                yAxis: false,
                gradient: false,
                legend: false,
                showXAxisLabel: false,
                xAxisLabel: "Days",
                showYAxisLabel: false,
                yAxisLabel: "Isues",
                scheme: {
                    domain: ["#42BFF7", "#C6ECFD", "#C7B42C", "#AAAAAA"],
                },
                curve: shape.curveBasis,
            },
        };
        this.estiloAgenciaDepartamento = {
            currentRange: "TW",
            legend: false,
            explodeSlices: false,
            labels: true,
            doughnut: true,
            gradient: false,
            scheme: {
                domain: ["#f44336", "#9c27b0", "#03a9f4", "#e91e63", "green"],
            },
            onSelect: (ev) => {
                console.log(ev);
            },
        };
        this.estiloAgenciasInfractoras = {
            domain: ["#4867d2", "#f44336", "green"],
        };
        this.estiloDepartamentosInfractores = {
            currentRange: "TW",
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: "Days",
            showYAxisLabel: false,
            yAxisLabel: "Isues",
            scheme: {
                domain: ["#C7B42C", "#AAAAAA", "#42BFF7", "#C6ECFD"],
            },
            onSelect: (ev) => {
                console.log(ev);
            },
            supporting: {
                currentRange: "",
                xAxis: false,
                yAxis: false,
                gradient: false,
                legend: false,
                showXAxisLabel: false,
                xAxisLabel: "Days",
                showYAxisLabel: false,
                yAxisLabel: "Isues",
                scheme: {
                    domain: ["#C7B42C", "#AAAAAA", "#42BFF7", "#C6ECFD"],
                },
                curve: shape.curveBasis,
            },
        };
        this.estiloAtencionDepartamento = {
            chartType: "line",
            colors: [
                {
                    borderColor: "#42a5f5",
                    backgroundColor: "#42a5f5",
                    pointBackgroundColor: "#1e88e5",
                    pointHoverBackgroundColor: "#1e88e5",
                    pointBorderColor: "#ffffff",
                    pointHoverBorderColor: "#ffffff",
                },
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false,
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 32,
                        left: 32,
                        right: 32,
                    },
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2,
                    },
                    line: {
                        tension: 0,
                    },
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                tickMarkLength: 18,
                            },
                            ticks: {
                                fontColor: "#ffffff",
                            },
                        },
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 1,
                                max: 7,
                                stepSize: 0.1,
                            },
                        },
                    ],
                },
                plugins: {
                    filler: {
                        propagate: false,
                    },
                    xLabelsOnTop: {
                        active: true,
                    },
                },
            },
        };
        this._registerCustomChartJSPlugin();
    }

    ngOnInit(): void {
        this.loading = false;
        this._cronogramaService.listar().subscribe((res) => {
            this.cronogramas = res;
        });
    }

    onChangeCronograma(idConfiguracion: number) {
        this.initialized = true;
        this.loading = true;
        this.reporteService.reporte(idConfiguracion).subscribe((res) => {
            this.reporteRecogidos = res.reporteRecogidos;
            this.reporteAgenciaDepartamento = res.agenciasxdepartamento;
            this.agenciasActivas = res.agenciasactivas;
            this.totalAgencias = res.totalagencias;
            this.reporteAgenciasInfractoras = res.reporteAgenciasInfractoras;
            this.reporteDepartamentosInfractores =
                res.reporteDepartamentosInfractores;
            this.reporteAtencionDepartamento = res.reporteAtencionDepartamento;
            this.reporteLabelAtencion = res.labels;
            this.loading = false;
            console.log(res);
        });
    }

    private _registerCustomChartJSPlugin(): void {
        (window as any).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing): any {
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop &&
                        chart.options.plugins.xLabelsOnTop.active === false)
                ) {
                    return;
                }
                const ctx = chart.ctx;
                chart.data.datasets.forEach(function (dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index): any {
                            ctx.fillStyle = "rgba(255, 255, 255, 0.7)";
                            const fontSize = 13;
                            const fontStyle = "normal";
                            const fontFamily = "Roboto, Helvetica Neue, Arial";
                            ctx.font = (window as any).Chart.helpers.fontString(
                                fontSize,
                                fontStyle,
                                fontFamily
                            );
                            const dataString =
                                dataset.data[index].toString() + "k";
                            ctx.textAlign = "center";
                            ctx.textBaseline = "middle";
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);
                            ctx.save();
                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = "rgba(255,255,255,0.12)";
                            ctx.stroke();
                            ctx.restore();
                        });
                    }
                });
            },
        });
    }
}
