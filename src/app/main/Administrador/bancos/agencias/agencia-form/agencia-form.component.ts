import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { AgenciaNuevaModel } from "app/models/agencia.model";
import { DistritoModel } from "app/models/distrito.model";
import { controllers } from "chart.js";
import { AgenciaService } from "../../../../../services/agencia.service";

interface Departamentos {
    departamentos: [];
}
interface Provincias {
    provincias: [];
}
interface Distritos {
    idDistrito: number;
    ubigeo: string;
    nombre: string;
    provincia: string;
    departamento: string;
    porcContagiados: number;
}
interface Actividad {
    value: string;
    viewValue: string;
}
@Component({
    selector: "app-agencia-form",
    templateUrl: "./agencia-form.component.html",
    styleUrls: ["./agencia-form.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class AgenciaFormComponent implements OnInit {
    form: FormGroup;
    dialogTitle: string;
    agencia = new AgenciaNuevaModel();
    distritoModel = new DistritoModel();

    idAgencia: string;
    idDistrito: string;
    idBanco: number;
    horaALV: string;
    horaAS: string;
    horaCLV: string;
    horaCS: string;

    mySubscriptionDep;
    mySubscriptionProv;
    mySubscriptionDist;

    //public agencia : Agencia[];
    public departamentos: string[];
    public provincias: string[];
    public distritos: Distritos[];

    selectedDep: string;
    selectedProv: string;
    selectedDist: string;

    estados: Actividad[] = [
        { value: "Activo", viewValue: "Activo" },
        { value: "Inactivo", viewValue: "Inactivo" },
    ];

    tipos = [{ value: "Cajero" }, { value: "Agente" }, { value: "Agencia" }];
    //departamentos = new FormControl('Departamentos');
    //departamentosList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

    constructor(
        public matDialogRef: MatDialogRef<AgenciaFormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar: MatSnackBar,
        private agenciaService: AgenciaService,
        private _formBuilder: FormBuilder
    ) {
        if (data.action === "editar") {
            this.dialogTitle = "Editar Agencia";
            this.agenciaService.consultar(this.data.idAgencia).subscribe(
                (res) => {
                    this.agencia = res;
                    console.log("res", JSON.stringify(res));
                    console.log("res", this.agencia);

                    this.form = this.agenciaForm();
                    this.selectedDep = res.distrito.departamento;
                    this.selectedProv = res.distrito.provincia;
                    this.selectedDist = res.distrito.nombre;
                    this.idAgencia = res.idLugarCubro;
                    this.idDistrito = res.distrito.idDistrito;
                    this.idBanco = res.entidadBancaria.idEntidadBancaria;
                    this.horaALV = res.horaAperturaLV;
                    this.horaAS = res.horaAperturaS;
                    this.horaCLV = res.horaCierreLV;
                    this.horaCS = res.horaCierreS;

                    console.log(
                        "selectes",
                        this.selectedDep,
                        this.selectedProv,
                        this.selectedDist
                    );
                },
                (err) => {
                    console.log("err", err);
                    this._matSnackBar.open("Error de consulta", "ok", {
                        verticalPosition: "bottom",
                        panelClass: ["my-snack-bar"],
                    });
                }
            );
        } else {
            this.dialogTitle = "Nueva Agencia";
        }
        this.form = this.agenciaForm();
    }

    ngOnInit(): void {
        this.mySubscriptionDep = this.agenciaService
            .listarDepartamentos()
            .subscribe((res) => {
                this.departamentos = res.departamentos;
            });
        console.log("deparramte", this.selectedDep);
        console.log("prov", this.selectedProv);
        console.log("dist", this.selectedDist);
        /*this.mySubscriptionProv = this.agenciaService.listarProvincias(this.selectedDep).subscribe(res => {
            this.provincias = res.provincias;
        });
        console.log("prov", this.selectedProv, this.provincias[0]);
        this.mySubscriptionDist = this.agenciaService.listarDistritos(this.selectedDep,this.selectedProv).subscribe(res => {
            this.distritos = res;
        });
        console.log("dist", this.selectedDist);
        //this.form.get('distrito').setValue("Lima");
        /*       
        this.form.reset({
            distrito:{
                idDistrito:1,
                nombre:"San Miguel"
            }
        });*/
        //const depDefault = this.departamentos.find(c => c === 'Lima');
        //this.form.get('departamento').setValue("Lima");
        //this.form.get('provincia').setValue("Lima");
        //this.form.get('distrito').setValue(1);
        //this.departamentos = this.agenciaService.listarDepartamentos();
        console.log("dep", this.agencia.departamento);
        console.log("veamos4", this.provincia.value);
        console.log("AGENCIA", JSON.stringify(this.agencia));

        //console.log("veamos4", this.selected);
    }

    agenciaForm(): FormGroup {
        return this._formBuilder.group({
            entidadBancaria: this._formBuilder.group({
                idEntidadBancaria: [this.data.id, Validators.required],
            }),
            idLugarCobro: [this.agencia.idLugarCobro],
            codigo: [
                this.agencia.codigo,
                [
                    Validators.required,
                    Validators.pattern("^[0-9]*$"),
                    Validators.maxLength(5),
                ],
            ],
            nombre: [this.agencia.nombre],
            departamento: [this.agencia.departamento],
            provincia: [this.agencia.provincia],
            distrito: this._formBuilder.group({
                idDistrito: [this.agencia.idDistrito],
            }),
            direccion: [this.agencia.direccion, Validators.required],
            horaAperturaLV: [this.agencia.horaAperturaLV, Validators.required],
            horaCierreLV: [this.agencia.horaCierreLV, Validators.required],
            horaAperturaS: [this.agencia.horaAperturaS],
            horaCierreS: [this.agencia.horaCierreS],
            numCajasAtencion: [
                this.agencia.numCajasAtencion,
                [Validators.required, Validators.pattern("^[0-9]*$")],
            ],
            atencionesPorHora: [
                this.agencia.atencionesPorHora,
                [Validators.required, Validators.pattern("^[0-9]*$")],
            ],
            tipo: [this.agencia.tipo],
            estado: [this.agencia.estado],
        });
    }

    crearAgencia(): void {
        this.form.patchValue({
            horaAperturaLV: this.horaAperturaLV.value + ":00",
            horaCierreLV: this.horaCierreLV.value + ":00",
            horaAperturaS: this.horaAperturaS.value + ":00",
            horaCierreS: this.horaCierreS.value + ":00",
        });
        console.log("veamos", this.form.value);
        this.agenciaService.crear(this.form.value).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                console.log("veamos", err);
                this._matSnackBar.open("Error, agencia no registrada", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
            }
        );
    }

    editarAgencia(): void {
        this.horaALV = this.horaAperturaLV.value;
        this.horaAS = this.horaAperturaS.value;
        this.horaCLV = this.horaCierreLV.value;
        this.horaCS = this.horaCierreS.value;

        this.form.patchValue({
            distrito: {
                idDistrito: this.idDistrito,
            },
            entidadBancaria: {
                idEntidadBancaria: this.idBanco,
            },
            departamento: this.selectedDep,
            provincia: this.selectedProv,
            horaAperturaLV:
                this.horaALV.length === 8 ? this.horaALV : this.horaALV + ":00",
            horaAperturaS:
                this.horaAS.length === 8 ? this.horaAS : this.horaAS + ":00",
            horaCierreLV:
                this.horaCLV.length === 8 ? this.horaCLV : this.horaCLV + ":00",
            horaCierreS:
                this.horaCS.length === 8 ? this.horaCS : this.horaCS + ":00",
        });
        console.log("form", JSON.stringify(this.form.value));
        this.agenciaService.editar(this.form.value).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                console.log("err", err);
                this._matSnackBar.open("Error, agencia no actualizado", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
            }
        );
    }

    get departamento() {
        return this.form.get("departamento");
    }
    get provincia() {
        return this.form.get("provincia");
    }
    get distrito() {
        return this.form.get("distrito.idDistrito");
    }
    get horaAperturaLV() {
        return this.form.get("horaAperturaLV");
    }
    get horaCierreLV() {
        return this.form.get("horaCierreLV");
    }
    get horaAperturaS() {
        return this.form.get("horaAperturaS");
    }
    get horaCierreS() {
        return this.form.get("horaCierreS");
    }
    get codigo() {
        return this.form.get("codigo");
    }
    get numCajasAtencion() {
        return this.form.get("numCajasAtencion");
    }
    get atencionesPorHora() {
        return this.form.get("atencionesPorHora");
    }

    codigoNoValidoMsj() {
        if (this.form.get("codigo").hasError("maxlength")) {
            return "No debe superar los 5 digitos";
        }
        return this.form.get("codigo").hasError("pattern")
            ? "Debe ser un número"
            : "";
    }
    numCajasAtencionNoValidoMsj() {
        return this.form.get("numCajasAtencion").hasError("pattern")
            ? "El valor debe ser un número"
            : "";
    }
    atencionesPorHoraNoValidoMsj() {
        return this.form.get("atencionesPorHora").hasError("pattern")
            ? "El valor debe ser un número"
            : "";
    }

    depOnChange() {
        console.log("Dep changed...");
        console.log(this.departamento.value);
        this.mySubscriptionProv = this.agenciaService
            .listarProvincias(this.departamento.value)
            .subscribe((res) => {
                this.provincias = res.provincias;
            });
    }
    provOnChange() {
        console.log("Prov changed...");
        console.log(this.provincia.value);
        this.mySubscriptionDist = this.agenciaService
            .listarDistritos(this.departamento.value, this.provincia.value)
            .subscribe((res) => {
                this.distritos = res;
            });
    }
    distOnChange() {
        console.log("Dist changed...");
        console.log(this.distrito.value);
        console.log("AGENCIA:", this.form.value);
    }
}
