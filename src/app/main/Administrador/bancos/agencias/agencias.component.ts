import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { fuseAnimations } from "@fuse/animations";
import { AgenciaMasivoComponent } from "./agencia-masivo/agencia-masivo.component";
import { AgenciaFormComponent } from "./agencia-form/agencia-form.component";
import { AgenciaService } from "../../../../services/agencia.service";
import { Router } from "@angular/router";
import { BancosComponent } from "../../bancos/bancos.component";
import { MessageBoxComponent } from "app/shared/message-box/message-box.component";
import { AgenciaModel, AgenciaNuevaModel } from "app/models/agencia.model";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
    selector: "app-agencias",
    templateUrl: "./agencias.component.html",
    styleUrls: ["./agencias.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class AgenciasComponent implements OnInit {
    dialogRef: any;
    visible: boolean;
    public dataSource = new MatTableDataSource<any>();

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator)
    set paginator(v: MatPaginator) {
        this.dataSource.paginator = v;
    }
    columnNames = [
        "codigo",
        "nombre",
        "departamento",
        "provincia",
        "distrito",
        //"direccion",
        "horario_lunes-viernes",
        "horario_sabado",
        "ncajas",
        "atencionesHora",
        "cantIncidencias",
        "estado",
        "editar",
        "eliminar",
    ];
    idBanco: number;
    nombreBanco: string;

    constructor(
        private agenciaService: AgenciaService,
        private _matDialog: MatDialog,
        private router: Router,
        private _matSnackBar: MatSnackBar
    ) {
        const navigation = this.router.getCurrentNavigation();
        const state = navigation.extras.state as { id: number; nombre: string };
        //localStorage.setItem("idBanco",state.id.toString());
        this.idBanco = parseInt(localStorage.getItem("idBanco"));
        this.nombreBanco = localStorage.getItem("nombreBanco");
        //console.log("veamos", state.nombre, state.id);
    }

    ngOnInit(): void {
        this.visible = false;
        this.agenciaService.listar(this.idBanco).subscribe((data) => {
            this.dataSource.data = data.content;
            this.dataSource.sort = this.sort;
            this.visible = true;
            console.log("data", data);
        });
    }

    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    };

    crear(): void {
        this.dialogRef = this._matDialog.open(AgenciaFormComponent, {
            panelClass: "agencia-form-dialog",
            data: {
                action: "nuevo",
                id: this.idBanco,
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    cargar(): void {
        this.dialogRef = this._matDialog.open(AgenciaMasivoComponent, {
            panelClass: "agencia-masivo-dialog",
            data: {
                action: "nuevo",
                id: this.idBanco,
            },
        });
        this.dialogRef.afterClosed().subscribe(() => {
            this.ngOnInit();
        });
    }

    editar(id: string): void {
        this.dialogRef = this._matDialog.open(AgenciaFormComponent, {
            panelClass: "agencia-form-dialog",
            data: {
                action: "editar",
                idAgencia: id,
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    eliminar(agencia: AgenciaNuevaModel): void {
        this.dialogRef = this._matDialog.open(MessageBoxComponent, {
            panelClass: "confirm-form-dialog",
            data: {
                titulo: "Eliminar Agencia: " + agencia.nombre,
                subTitulo: "¿Está seguro de eliminar la agencia?",
                botonAceptar: "Si",
                botonCancelar: "No",
            },
        });

        console.log("eliminaAgencia", JSON.stringify(agencia));

        this.dialogRef.afterClosed().subscribe((result) => {
            if (localStorage.getItem("eliminar") === "S") {
                this.agenciaService.eliminar(agencia).subscribe(
                    (data) => {
                        console.log("data", data);
                        this._matSnackBar.open("Se eliminó la agencia", "", {
                            verticalPosition: "top",
                            horizontalPosition: "center",
                            duration: 5000,
                        });
                        this.ngOnInit();
                    },
                    (err) => {
                        console.log("err", err);
                        this._matSnackBar.open(
                            "Error, agencia no eliminada",
                            "",
                            {
                                verticalPosition: "top",
                                duration: 5000,
                            }
                        );
                    }
                );
            }
            localStorage.setItem("eliminarAgencia", "N");
        });
    }
}
