import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { fuseAnimations } from "@fuse/animations";
import { BancoService } from "../../../services/banco.service";
import { Router } from "@angular/router";
import { BancoFormComponent } from "./banco-form/banco-form.component";
import { MatDialog } from "@angular/material/dialog";
import { BancoModel } from "app/models/banco.model";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MessageBoxComponent } from "app/shared/message-box/message-box.component";
@Component({
    selector: "app-bancos",
    templateUrl: "./bancos.component.html",
    styleUrls: ["./bancos.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class BancosComponent implements OnInit {
    public dataSource = new MatTableDataSource<any>();
    idBanco: number;
    visible: boolean;

    dialogRef: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator)
    set paginator(v: MatPaginator) {
        this.dataSource.paginator = v;
    }
    columnNames = [
        "idEntidadBancaria",
        "ruc",
        "nombre",
        "estado",
        "agencias",
        "editar",
        "eliminar",
    ];

    constructor(
        private bancoService: BancoService,
        private router: Router,
        private _matDialog: MatDialog,
        private _matSnackBar: MatSnackBar
    ) {}

    ngOnInit(): void {
        this.visible = false;
        this.bancoService.listar().subscribe((res) => {
            this.dataSource.data = res;
            this.dataSource.sort = this.sort;
            this.visible = true;
        });
    }

    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    };

    crear(): void {
        this.dialogRef = this._matDialog.open(BancoFormComponent, {
            panelClass: "banco-form-dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    verAgencias(banco: BancoModel): void {
        this.idBanco = banco.idEntidadBancaria;
        localStorage.setItem("idBanco", banco.idEntidadBancaria.toString());
        localStorage.setItem("nombreBanco", banco.nombre);
        this.router.navigate(
            [`admin/agencias/${parseInt(localStorage.getItem("idBanco"))}`],
            {
                state: {
                    id: parseInt(localStorage.getItem("idBanco")),
                    nombre: localStorage.getItem("nombreBanco"),
                },
            }
        );
    }

    editar(id: number): void {
        this.dialogRef = this._matDialog.open(BancoFormComponent, {
            panelClass: "banco-form-dialog",
            data: {
                action: "editar",
                id: id,
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    eliminar(banco: BancoModel): void {
        this.dialogRef = this._matDialog.open(MessageBoxComponent, {
            panelClass: "confirm-form-dialog",
            data: {
                titulo: "Eliminar banco: " + banco.nombre,
                subTitulo: "¿Está seguro de eliminar el banco?",
                botonAceptar: "Si",
                botonCancelar: "No",
            },
        });

        this.dialogRef.afterClosed().subscribe((result) => {
            if (localStorage.getItem("eliminar") === "S") {
                this.bancoService.eliminar(banco).subscribe(
                    (data) => {
                        this._matSnackBar.open("Se eliminó el banco", "", {
                            verticalPosition: "top",
                            horizontalPosition: "center",
                            duration: 5000,
                        });
                        this.ngOnInit();
                    },
                    (err) => {
                        this._matSnackBar.open(
                            "Error, banco no eliminado",
                            "",
                            {
                                verticalPosition: "top",
                                duration: 5000,
                            }
                        );
                    }
                );
            }
            localStorage.setItem("eliminar", "N");
        });
    }
}
