import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { BancoModel } from "app/models/banco.model";
import { BancoService } from "../../../../services/banco.service";
interface Actividad {
    value: string;
    viewValue: string;
}
@Component({
    selector: "app-banco-form",
    templateUrl: "./banco-form.component.html",
    styleUrls: ["./banco-form.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class BancoFormComponent implements OnInit {
    form: FormGroup;
    dialogTitle: string;
    banco = new BancoModel();
    estados: Actividad[] = [
        { value: "Activo", viewValue: "Activo" },
        { value: "Inactivo", viewValue: "Inactivo" },
    ];

    constructor(
        public matDialogRef: MatDialogRef<BancoFormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar: MatSnackBar,
        private bancoService: BancoService,
        private _formBuilder: FormBuilder
    ) {
        if (data.action === "editar") {
            this.dialogTitle = "Editar Banco";
            this.bancoService.consultar(data.id).subscribe(
                (res) => {
                    this.banco = res;
                    this.form = this.bancoForm();
                },
                (err) => {
                    this._matSnackBar.open("Error de consulta", "ok", {
                        verticalPosition: "bottom",
                        panelClass: ["my-snack-bar"],
                    });
                }
            );
        } else {
            this.dialogTitle = "Nuevo Banco";
        }
        this.form = this.bancoForm();
    }

    ngOnInit(): void {}

    bancoForm(): FormGroup {
        return this._formBuilder.group({
            idEntidadBancaria: [this.banco.idEntidadBancaria],
            ruc: [
                this.banco.ruc,
                [
                    Validators.pattern("^[0-9]*$"),
                    Validators.minLength(11),
                    Validators.maxLength(11),
                ],
            ],
            nombre: [this.banco.nombre],
            estado: [this.banco.estado],
        });
    }

    crearBanco(): void {
        this.form.patchValue({
            estado: "Activo",
        });
        console.log("veamos", this.form.value);
        this.bancoService.crear(this.form.value).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                if (err.error.mensaje === "RUC repetido") {
                    this._matSnackBar.open("Error, ruc repetido", "", {
                        verticalPosition: "top",
                        duration: 5000,
                    });
                } else {
                    this._matSnackBar.open("Error, banco no registrado", "", {
                        verticalPosition: "top",
                        duration: 5000,
                    });
                }
            }
        );
    }

    editarBanco(): void {
        this.bancoService.editar(this.form.value).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                if (err.error.mensaje === "RUC repetido") {
                    this._matSnackBar.open("Error, ruc repetido", "", {
                        verticalPosition: "top",
                        duration: 5000,
                    });
                } else {
                    this._matSnackBar.open("Error, banco no actualizado", "", {
                        verticalPosition: "top",
                        duration: 5000,
                    });
                }
            }
        );
    }
}
