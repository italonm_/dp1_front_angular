import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { DistritoService } from "app/services/distrito.service";

@Component({
    selector: "app-distrito-masivo",
    templateUrl: "./distrito-masivo.component.html",
    styleUrls: ["./distrito-masivo.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class DistritoMasivoComponent implements OnInit {
    form: FormGroup;
    visible: boolean;
    nregistros: number;
    nerrores: number;
    constructor(
        public matDialogRef: MatDialogRef<DistritoMasivoComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar: MatSnackBar,
        private _distritoService: DistritoService,
        private _formBuilder: FormBuilder
    ) {
        this.form = this._formBuilder.group({ basicfile: new FormControl() });
    }

    ngOnInit(): void {}

    crearDistrito(): void {
        this.visible = false;
        const formData = new FormData();
        formData.append("file", this.form.get("basicfile").value._files[0]);
        this._distritoService.crearMasivo(formData).subscribe(
            (data) => {
                if (data.type === "application/csv") {
                    const a = document.createElement("a");
                    const objectUrl = URL.createObjectURL(data);
                    a.href = objectUrl;
                    a.download = "errores.csv";
                    a.click();
                    URL.revokeObjectURL(objectUrl);
                    this.mostrarMensaje(
                        data,
                        this.form.get("basicfile").value._files[0]
                    );
                } else {
                    this._matSnackBar.open("Carga masiva exitosa", "", {
                        verticalPosition: "top",
                        horizontalPosition: "center",
                        duration: 5000,
                    });
                }
                this.visible = true;
                this.matDialogRef.close();
            },
            (err) => {
                var reader = new FileReader();
                reader.onload = (e) => {
                    this._matSnackBar.open(
                        JSON.parse(reader.result.toString()).mensaje,
                        "",
                        {
                            verticalPosition: "top",
                            horizontalPosition: "center",
                            duration: 5000,
                            panelClass: "red-snackbar",
                        }
                    );
                };
                reader.readAsText(err.error);
                this.visible = true;
            }
        );
    }

    descargarPlantilla(): void {
        this._distritoService.descargar().subscribe((data) => {
            const a = document.createElement("a");
            const objectUrl = URL.createObjectURL(data);
            a.href = objectUrl;
            a.download = "plantilla_distritos.csv";
            a.click();
            URL.revokeObjectURL(objectUrl);
        });
    }

    mostrarMensaje(errores: any, registros: any): void {
        const readerErrores = new FileReader();
        readerErrores.onload = (e) => {
            let results = "";
            results += readerErrores.result;
            const lines = results.split("\n");
            lines.pop();
            this.nerrores = lines.length - 1;
            const message =
                "Registros exitosos: " +
                (this.nregistros - this.nerrores) +
                "\nRegistros con mal formato: " +
                this.nerrores +
                "\nDescargue el archivo plantilla";
            this._matSnackBar.open(message, "", {
                verticalPosition: "top",
                horizontalPosition: "center",
                duration: 5000,
                panelClass: "red-snackbar",
            });
        };

        const readerRegistros = new FileReader();
        readerRegistros.onload = (e) => {
            let results = "";
            results += readerRegistros.result;
            const lines = results.split("\n");
            lines.pop();
            this.nregistros = lines.length - 1;
            readerErrores.readAsText(errores);
        };
        readerRegistros.readAsText(registros);
    }
}
