import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DistritoMasivoComponent } from './distrito-masivo.component';

describe('DistritoMasivoComponent', () => {
  let component: DistritoMasivoComponent;
  let fixture: ComponentFixture<DistritoMasivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DistritoMasivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DistritoMasivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
