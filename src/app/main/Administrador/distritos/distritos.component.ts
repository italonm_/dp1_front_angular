import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { fuseAnimations } from "@fuse/animations";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MessageBoxComponent } from "app/shared/message-box/message-box.component";
import { DistritoService } from "app/services/distrito.service";
import { DistritoModel } from "app/models/distrito.model";
import { DistritoFormComponent } from "../distritos/distrito-form/distrito-form.component";
import { DistritoMasivoComponent } from "./distrito-masivo/distrito-masivo.component";

@Component({
    selector: "app-distritos",
    templateUrl: "./distritos.component.html",
    styleUrls: ["./distritos.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class DistritosComponent implements OnInit {
    public dataSource = new MatTableDataSource<any>();
    visible: boolean;
    dialogRef: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator)
    set paginator(v: MatPaginator) {
        this.dataSource.paginator = v;
    }
    //falta cambiar
    columnNames = [
        "departamento",
        "provincia",
        "nombre",
        "porcContagiados",
        "ubigeo",
        "editar",
    ];

    constructor(
        private DistritoService: DistritoService,
        private router: Router,
        private _matDialog: MatDialog,
        private _matSnackBar: MatSnackBar
    ) {}

    ngOnInit(): void {
        this.visible = false;
        this.DistritoService.listar().subscribe((res) => {
            this.visible = true;
            this.dataSource.data = res;
            this.dataSource.sort = this.sort;
        });
    }

    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    };

    crear(): void {
        this.dialogRef = this._matDialog.open(DistritoFormComponent, {
            panelClass: "distrito-form-dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    cargar(): void {
        this.dialogRef = this._matDialog.open(DistritoMasivoComponent, {
            panelClass: "distrito-masivo-dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe(() => {
            this.ngOnInit();
        });
    }

    editar(id: number): void {
        this.dialogRef = this._matDialog.open(DistritoFormComponent, {
            panelClass: "distrito-form-dialog",
            data: {
                action: "editar",
                id: id,
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    eliminar(distrito: DistritoModel): void {
        this.dialogRef = this._matDialog.open(MessageBoxComponent, {
            panelClass: "confirm-form-dialog",
            data: {
                titulo: "Eliminar distrito: " + distrito.nombre,
                subTitulo: "¿Está seguro de eliminar el distrito?",
                botonAceptar: "Si",
                botonCancelar: "No",
            },
        });

        this.dialogRef.afterClosed().subscribe((result) => {
            if (localStorage.getItem("eliminar") === "S") {
                this.DistritoService.eliminar(distrito).subscribe(
                    (data) => {
                        this._matSnackBar.open("Se eliminó el distrito", "", {
                            verticalPosition: "top",
                            horizontalPosition: "center",
                            duration: 5000,
                        });
                        this.ngOnInit();
                    },
                    (err) => {
                        this._matSnackBar.open(
                            "Error, distrito no eliminado",
                            "",
                            {
                                verticalPosition: "top",
                                duration: 5000,
                            }
                        );
                    }
                );
            }
            localStorage.setItem("eliminar", "N");
        });
    }
}
