import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { DistritoModel } from "app/models/distrito.model";
import { DistritoService } from "../../../../services/distrito.service";
import { AgenciaService } from "../../../../services/agencia.service";

interface Departamentos {
    departamentos:[]
}
interface Provincias {
    provincias:[]
}
interface Distritos {
    idDistrito: number,
    ubigeo: string,
    nombre: string,
    provincia: string,
    departamento: string,
    porcContagiados: number,
}
interface Actividad {
    value: string;
    viewValue: string;
}
@Component({
    selector: "app-distrito-form",
    templateUrl: "./distrito-form.component.html",
    styleUrls: ["./distrito-form.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class DistritoFormComponent implements OnInit {
    dialogRef: any;
    form: FormGroup;
    dialogTitle: string;
    distrito = new DistritoModel();
    mySubscriptionDep;
    mySubscriptionProv;
    mySubscriptionDist;

    //public agencia : Agencia[];
    public departamentos : string[];
    public provincias : string[];
    public distritos : Distritos[];

    estados: Actividad[] = [
        { value: "Activo", viewValue: "Activo" },
        { value: "Inactivo", viewValue: "Inactivo" },
    ];

    constructor(
        private _matDialog: MatDialog,
        public matDialogRef: MatDialogRef<DistritoFormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar: MatSnackBar,
        private distritoService: DistritoService,
        private agenciaService: AgenciaService,
        private _formBuilder: FormBuilder
    ) {
        if (data.action === "editar") {
            this.dialogTitle = "Editar Distrito";
            this.distritoService.consultar(data.id).subscribe(
                (res) => {
                    this.distrito = res;
                    this.form = this.distritoForm();
                },
                (err) => {
                    this._matSnackBar.open("Error de consulta", "ok", {
                        verticalPosition: "bottom",
                        panelClass: ["my-snack-bar"],
                    });
                }
            );
        } else {
            this.dialogTitle = "Nuevo Distrito";
        }
        this.form = this.distritoForm();
    }

    ngOnInit(): void {
        this.mySubscriptionDep = this.agenciaService.listarDepartamentos().subscribe(res => {
            this.departamentos = res.departamentos;
        });
    }

    distritoForm(): FormGroup {
        return this._formBuilder.group({
            idDistrito: [this.distrito.idDistrito,Validators.required],
            departamento: [this.distrito.departamento,Validators.required],
            provincia: [this.distrito.provincia,Validators.required],
            nombre: [this.distrito.nombre,Validators.required],
            porcContagiados: [this.distrito.porcContagiados,
                [Validators.required,Validators.pattern("\\d+\\.\\d{2}"),Validators.max(100)]],
            ubigeo: [this.distrito.ubigeo,Validators.required],
        });
    }/*
    cargar(): void { 
        this.dialogRef = this._matDialog.open(DistritoMasivoComponent, {
            panelClass: "distrito-masivo-dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe(() => {
            this.ngOnInit();
        });
    }
*/
    
    crearDistrito(): void {
        console.log("veamos",this.form.value);
        this.distritoService.crear(this.form.value).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                this._matSnackBar.open("Error, distrito no registrado", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
            }
        );
    }

    editarDistrito(): void {
        this.distritoService.editar(this.form.value).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                this._matSnackBar.open("Error, distrito no actualizado", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
            }
        );
    }

    get departamento() {
        return this.form.get('departamento');
    }
    get provincia() {
    return this.form.get('provincia');
    }
    get nombre() {
    return this.form.get('nombre');    
    }
    get porcContagiados() {
        return this.form.get('porcContagiados');    
        }
    porcContagiadosNoValidoMsj() {
        if (this.form.get('porcContagiados').hasError('max')){
            return 'No debe superar el 100%';
        }
        return this.form.get('porcContagiados').hasError('pattern') ? 'Número inválido' : '';
    }

    depOnChange() {
    console.log('Dep changed...');
    console.log(this.departamento.value);
    this.mySubscriptionProv = this.agenciaService.listarProvincias(this.departamento.value).subscribe(res => {
        this.provincias = res.provincias;
    });
  }
  provOnChange() {
    console.log('Prov changed...');
    console.log(this.provincia.value);
    this.mySubscriptionDist = this.agenciaService.listarDistritos(this.departamento.value,this.provincia.value).subscribe(res => {
        this.distritos = res;
    });
  }
  distOnChange() {
    console.log('Dist changed...');
    console.log(this.nombre.value);
    console.log("AGENCIA:",this.form.value);
  }

}
