
import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { MotivoIncidenciaModel } from "app/models/motivoIncidencia.model";
import { IncidenciaService } from "../../../../services/motivoIncidencia.service";
interface Tipo {
    value: string;
    viewValue: string;
}

@Component({
    selector: "app-incidente-form",
    templateUrl: "./incidente-form.component.html",
    styleUrls: ["./incidente-form.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class IncidenciaFormComponent implements OnInit {
    form: FormGroup;
    dialogTitle: string;
    incidencia = new MotivoIncidenciaModel();

    tipos: Tipo[] = [
        { value: "Lugar de Cobro", viewValue: "Lugar de Cobro" },
        { value: "Beneficiarios", viewValue: "Beneficiarios" },
    ];

    constructor(
        public matDialogRef: MatDialogRef<IncidenciaFormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar: MatSnackBar,
        private incidenciaService: IncidenciaService,
        private _formBuilder: FormBuilder
    ) {
        if (data.action === "editar") {
            this.dialogTitle = "Editar Incidencia";
            this.incidenciaService.consultar(data.id).subscribe(
                (res) => {
                    this.incidencia = res;
                    this.form = this.incidenciaForm();
                    console.log("res",res);
                },
                (err) => {
                    this._matSnackBar.open("Error de consulta", "ok", {
                        verticalPosition: "bottom",
                        panelClass: ["my-snack-bar"],
                    });
                }
            );
        } else {
            this.dialogTitle = "Nuevo Incidencia";
        }
        this.form = this.incidenciaForm();
    }

    ngOnInit(): void {}

    incidenciaForm(): FormGroup {
        return this._formBuilder.group({
            idMotivoIncidencia: [this.incidencia.idMotivoIncidencia],
            nombre: [this.incidencia.nombre],
            tipo: [this.incidencia.tipo],
        });
    }

    crearIncidencia(): void {
        console.log("veamos",this.form.value);
        this.incidenciaService.crear(this.form.value).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                this._matSnackBar.open("Error, incidencia no registrado", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
            }
        );
    }

    editarIncidencia(): void {
        this.incidenciaService.editar(this.form.value).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                this._matSnackBar.open("Error, incidencia no actualizado", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
            }
        );
    }
}
