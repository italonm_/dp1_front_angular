import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { fuseAnimations } from "@fuse/animations";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MessageBoxComponent } from "app/shared/message-box/message-box.component";
import { IncidenciaService } from "app/services/motivoIncidencia.service";
import { IncidenciaFormComponent } from "./incidente-form/incidente-form.component";
import { MotivoIncidenciaModel } from "app/models/motivoIncidencia.model";

@Component({
    selector: "app-incidentes",
    templateUrl: "./incidentes.component.html",
    styleUrls: ["./incidentes.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class IncidentesComponent implements OnInit {
    public dataSource = new MatTableDataSource<any>();
    visible: boolean;
    dialogRef: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator)
    set paginator(v: MatPaginator) {
        this.dataSource.paginator = v;
    }
    //falta cambiar
    columnNames = ["ID", "nombre", "tipo", "editar", "eliminar"];

    constructor(
        private IncidenciaService: IncidenciaService,
        private router: Router,
        private _matDialog: MatDialog,
        private _matSnackBar: MatSnackBar
    ) {}

    ngOnInit(): void {
        this.visible = false;
        this.IncidenciaService.listar().subscribe((res) => {
            console.log(res);
            this.visible = true;
            this.dataSource.data = res;
            this.dataSource.sort = this.sort;
        });
    }

    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    };

    crear(): void {
        this.dialogRef = this._matDialog.open(IncidenciaFormComponent, {
            panelClass: "incidente-form-dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    editar(id: number): void {
        this.dialogRef = this._matDialog.open(IncidenciaFormComponent, {
            panelClass: "incidente-form-dialog",
            data: {
                action: "editar",
                id: id,
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    eliminar(incidencia: MotivoIncidenciaModel): void {
        this.dialogRef = this._matDialog.open(MessageBoxComponent, {
            panelClass: "confirm-form-dialog",
            data: {
                titulo: "Eliminar incidencia: " + incidencia.nombre,
                subTitulo: "¿Está seguro de eliminar el motivo de incidencia?",
                botonAceptar: "Si",
                botonCancelar: "No",
            },
        });

        this.dialogRef.afterClosed().subscribe((result) => {
            if (localStorage.getItem("eliminar") === "S") {
                this.IncidenciaService.eliminar(incidencia).subscribe(
                    (data) => {
                        this._matSnackBar.open("Se eliminó la incidencia", "", {
                            verticalPosition: "top",
                            horizontalPosition: "center",
                            duration: 5000,
                        });
                        this.ngOnInit();
                    },
                    (err) => {
                        console.log("err", err);
                        this._matSnackBar.open(
                            "Error, Incidencia no eliminada",
                            "",
                            {
                                verticalPosition: "top",
                                duration: 5000,
                            }
                        );
                    }
                );
            }
            localStorage.setItem("eliminar", "N");
        });
    }
}
