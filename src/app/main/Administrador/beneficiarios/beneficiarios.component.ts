import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { fuseAnimations } from "@fuse/animations";
import { BeneficiarioService } from "../../../services/beneficiario.service";
import { Router } from "@angular/router";
import { BeneficiarioMasivoComponent } from "./beneficiario_masivo/beneficiario-masivo.component";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
    selector: "app-beneficiarios",
    templateUrl: "./beneficiarios.component.html",
    styleUrls: ["./beneficiarios.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class BeneficiariosComponent implements OnInit {
    public dataSource = new MatTableDataSource<any>();
    pageIndex: any;
    pageSize: any;
    length: number;
    visible: boolean;

    dialogRef: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator)
    set paginator(v: MatPaginator) {
        // this.dataSource.paginator = v;
    }
    columnNames = [
        "codigoHogar",
        "flagMayorEdad",
        "flagDiscapacitado",
        "genero",
        "departamento",
        "provincia",
        "distrito",
    ];
    constructor(
        private beneficiarioService: BeneficiarioService,
        private router: Router,
        private _matDialog: MatDialog
    ) {}

    ngOnInit(): void {
        this.visible = false;
        // this.beneficiarioService.listar().subscribe((res) => {
        //     this.dataSource.data = res.content;
        //     this.visible = true;
        //     this.dataSource.sort = this.sort;
        // });
        this.listar(null);
    }

    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    };

    crear(): void {
        this.dialogRef = this._matDialog.open(BeneficiarioMasivoComponent, {
            panelClass: "beneficiario-form--dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe(() => {
            this.ngOnInit();
        });
    }

    public listar(event?: PageEvent) {
        this.visible = false;
        console.log(event);
        if (event === null) {
            event = new PageEvent();
            event.pageIndex = 0;
            event.pageSize = 5;
        }
        this.beneficiarioService
            .listar(event.pageIndex, event.pageSize)
            .subscribe((res) => {
                this.visible = true;
                console.log(res);
                this.dataSource.data = res.content;
                this.pageIndex = res.number;
                this.pageSize = res.size;
                this.length = res.totalElements;
            });
        return event;
    }
}
