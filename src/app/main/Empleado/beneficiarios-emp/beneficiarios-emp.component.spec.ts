import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiariosEmpComponent } from './beneficiarios-emp.component';

describe('BeneficiariosEmpComponent', () => {
  let component: BeneficiariosEmpComponent;
  let fixture: ComponentFixture<BeneficiariosEmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeneficiariosEmpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiariosEmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
