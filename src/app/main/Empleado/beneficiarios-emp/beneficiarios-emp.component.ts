import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { fuseAnimations } from "@fuse/animations";
import { BeneficiarioService } from "../../../services/beneficiario.service";
import { EmpleadoService } from "../../../services/empleado.service";
import { EmpleadoModel } from "app/models/empleado.model";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatDialog } from "@angular/material/dialog";
import { MessageBoxComponent } from "app/shared/message-box/message-box.component";
import { IncidenteEmpFormComponent } from "../incidentes-emp/incidente-emp-form/incidente-emp-form.component";

@Component({
    selector: "app-beneficiarios-emp",
    templateUrl: "./beneficiarios-emp.component.html",
    styleUrls: ["./beneficiarios-emp.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class BeneficiariosEmpComponent implements OnInit {
    public dataSource = new MatTableDataSource<any>();
    visible: boolean;
    dialogRef: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator)
    set paginator(v: MatPaginator) {
        this.dataSource.paginator = v;
    }
    columnNames = [
        "hora",
        "codigoHogar",
        "flagMayorEdad",
        "flagDiscapacitado",
        "genero",
        "incidencias",
        "estado",
    ];
    idLugarCobro: string;
    idEmpleado: string;
    nombreLugarCobro: string;
    data: EmpleadoModel;
    tamanho: number;

    constructor(
        private beneficiarioService: BeneficiarioService,
        private _matSnackBar: MatSnackBar,
        private _matDialog: MatDialog
    ) {
        this.idEmpleado = localStorage.getItem("usuarioId");
        this.idLugarCobro = localStorage.getItem("agenciaId");
        this.nombreLugarCobro = localStorage.getItem("agenciaNombre");
    }

    ngOnInit(): void {
        this.visible = false;
        this.beneficiarioService
            .listarPorAgencia(this.idLugarCobro)
            .subscribe((res) => {
                this.visible = true;
                this.dataSource.data = res;
                this.dataSource.sort = this.sort;
            });
    }

    updateEntregado(beneficiarioAgencia) {
        const idConfiguracion = beneficiarioAgencia.idConfiguracion;
        const idBeneficiario = beneficiarioAgencia.beneficiario.idBeneficiario;
        const date: Date = new Date();
        if (
            date.getTime() > new Date(beneficiarioAgencia.fechaIni).getTime() &&
            date.getTime() < new Date(beneficiarioAgencia.fechaFin).getTime()
        ) {
            this.dialogRef = this._matDialog.open(MessageBoxComponent, {
                panelClass: "confirm-form-dialog",
                data: {
                    titulo: "Confirmar la entrega ",
                    subTitulo: "¿Está seguro de registrar la entrega del bono?",
                    botonAceptar: "Si",
                    botonCancelar: "No",
                },
            });
            this.dialogRef.afterClosed().subscribe((result) => {
                if (localStorage.getItem("eliminar") === "S") {
                    this.beneficiarioService
                        .asistir(idConfiguracion, idBeneficiario)
                        .subscribe(
                            (res) => {
                                this._matSnackBar.open(
                                    "Recogo de bono registrado",
                                    "",
                                    {
                                        verticalPosition: "top",
                                        duration: 5000,
                                    }
                                );
                                this.ngOnInit();
                            },
                            (err) => {
                                this._matSnackBar.open(
                                    "Error, no se pudo marcar la asistencia",
                                    "",
                                    {
                                        verticalPosition: "top",
                                        duration: 5000,
                                    }
                                );
                            }
                        );
                }
                localStorage.setItem("eliminar", "N");
            });
        } else {
            this.dialogRef = this._matDialog.open(MessageBoxComponent, {
                panelClass: "confirm-form-dialog",
                data: {
                    titulo: "Error, el beneficiario asistió fuera del plazo ",
                    subTitulo: "¿Desea registrar una incidencia?",
                    botonAceptar: "Si",
                    botonCancelar: "No",
                },
            });
            this.dialogRef.afterClosed().subscribe((result) => {
                if (localStorage.getItem("eliminar") === "S") {
                    this.dialogRef = this._matDialog.open(
                        IncidenteEmpFormComponent,
                        {
                            panelClass: "incidente-emp-form-dialog",
                            data: {
                                action: "nuevo",
                                beneficiario:
                                    beneficiarioAgencia.beneficiario
                                        .codigoHogar,
                            },
                        }
                    );
                }
                localStorage.setItem("eliminar", "N");
            });
        }
    }

    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    };
}
