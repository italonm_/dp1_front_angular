import {
    Component,
    Inject,
    OnInit,
    ViewChild,
    ViewEncapsulation,
} from "@angular/core";
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    Validators,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { AgenciaModel } from "app/models/agencia.model";
import { BancoModel } from "app/models/banco.model";
import { IncidenteModel } from "app/models/incidente.model";
import { AgenciaService } from "app/services/agencia.service";
import { BancoService } from "app/services/banco.service";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { IncidenteService } from "../../../../services/incidente.service";

interface Motivo {
    idMotivoIncidencia: number;
    ubigeo: string;
    nombre: string;
    tipo: string;
    estado: string;
    _onChange: (number) => number;
}

@Component({
    selector: "app-incidente-emp-form",
    templateUrl: "./incidente-emp-form.component.html",
    styleUrls: ["./incidente-emp-form.component.scss"],
    encapsulation: ViewEncapsulation.None,
    providers: [MatSnackBar, MatSnackBarConfig],
})
export class IncidenteEmpFormComponent implements OnInit {
    form: FormGroup;
    dialogTitle: string;
    incidencia = new IncidenteModel();
    mySubscriptionMotivos;

    public motivos: Motivo[];
    idEmpleado: string;
    //-----beneficiario-----
    esBeneficiario: boolean;
    idLugarCobro: string;
    //selectedMotivo: number;
    error: boolean;
    //configuracion
    idConfiguracion: number;
    @ViewChild("selectedMotivo") public selectedMotivo: Motivo;

    constructor(
        public matDialogRef: MatDialogRef<IncidenteEmpFormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _matSnackBar: MatSnackBar,
        private incidenteService: IncidenteService,
        private _formBuilder: FormBuilder
    ) {
        if (data.action === "editar") {
            this.dialogTitle = "Editar Incidencia";
            this.incidenteService.consultar(data.id).subscribe(
                (res) => {
                    this.incidencia = res;
                    this.incidencia.codigoHogar = res.beneficiario.codigoHogar;
                    this.form = this.incidenciaForm();
                    this.idLugarCobro = res.lugarCobro.idLugarCobro;
                    console.log("incidencia", JSON.stringify(res));
                    this.idConfiguracion =
                        res.configuracionCronograma.idConfiguracion;
                    this.mySubscriptionMotivos = this.incidenteService
                        .listarMotivos(
                            this.esBeneficiario ? "lugarcobro" : "beneficiario"
                        )
                        .subscribe((res2) => {
                            this.motivos = res2;
                            this.selectedMotivo._onChange(
                                res.motivoIncidencia.idMotivoIncidencia
                            );
                        });
                },
                (err) => {
                    this._matSnackBar.open("Error de consulta", "ok", {
                        verticalPosition: "bottom",
                        panelClass: ["my-snack-bar"],
                    });
                }
            );
        } else {
            if (data.action === "nuevaIncBenef") {
                this.esBeneficiario = true;
                this.idLugarCobro = data.idLugarCobro;
                this.incidencia.codigoHogar = data.beneficiario;
            }
            if (data.beneficiario) {
                this.incidencia.codigoHogar = data.beneficiario;
            }
            this.dialogTitle = "Nuevo Incidencia";
            this.mySubscriptionMotivos = this.incidenteService
                .listarMotivos(
                    this.esBeneficiario ? "lugarcobro" : "beneficiario"
                )
                .subscribe((res) => {
                    this.motivos = res;
                    this.selectedMotivo._onChange(res[0].idMotivoIncidencia);
                    console.log("this.selectedMotivo", this.selectedMotivo);
                    console.log(this.esBeneficiario);
                });
        }
        this.form = this.incidenciaForm();
        this.idEmpleado = localStorage.getItem("usuarioId");
    }

    ngOnInit(): void {}

    incidenciaForm(): FormGroup {
        return this._formBuilder.group({
            idIncidencia: [this.incidencia.idIncidencia],
            descripcion: [this.incidencia.descripcion, Validators.required],
            codigoHogar: [
                this.esBeneficiario
                    ? parseInt(localStorage.getItem("codigoBeneficiario"))
                    : this.incidencia.codigoHogar,
                [Validators.pattern("^[0-9]*$"), Validators.required],
            ],
            idMotivoIncidencia: [this.incidencia.idMotivoIncidencia],
            idUsuario: [this.idEmpleado],
            idLugarCobro: [localStorage.getItem("agenciaId")],
            idConfiguracion: [""],
            fecha: [this.incidencia.fecha],
        });
    }
    _OnChange(id: number) {
        this.form.patchValue({
            idMotivoIncidencia: id,
        });
    }

    crearIncidencia(): void {
        let data = new IncidenteModel();
        let tipo = 1;
        if (this.esBeneficiario) {
            tipo = 2;
            data.idLugarCobro = this.idLugarCobro;
            data.codigoHogar = this.form.value.codigoHogar;
            data.idMotivoIncidencia = this.form.value.idMotivoIncidencia;
            data.descripcion = this.form.value.descripcion;
        } else {
            this.form.patchValue({
                idUsuario: this.idEmpleado,
            });
            data = this.form.value;
        }
        console.log("veamos", data);
        this.incidenteService.crear(data, tipo).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                if (err.error) {
                    this._matSnackBar.open("El código no existe", "", {
                        verticalPosition: "top",
                        duration: 5000,
                    });
                } else {
                    this._matSnackBar.open(
                        "Error, incidencia no registrada",
                        "",
                        {
                            verticalPosition: "top",
                            duration: 5000,
                        }
                    );
                }

                console.log("mensaje", err.error.mensaje);
            }
        );
    }

    editarIncidencia(): void {
        let data = new IncidenteModel();
        data.idIncidencia = this.form.value.idIncidencia;
        data.idLugarCobro = this.idLugarCobro;
        data.codigoHogar = this.form.value.codigoHogar;
        data.idMotivoIncidencia = this.form.value.idMotivoIncidencia;
        data.descripcion = this.form.value.descripcion;
        data.idConfiguracion = this.idConfiguracion;

        console.log("data", data);
        this.incidenteService.editar(data, 1).subscribe(
            (res) => {
                this._matSnackBar.open("Grabación exitosa", "", {
                    verticalPosition: "top",
                    duration: 5000,
                });
                this.matDialogRef.close();
            },
            (err) => {
                console.log(err);
                if (err.error) {
                    this._matSnackBar.open("El código no existe", "", {
                        verticalPosition: "top",
                        duration: 5000,
                    });
                } else {
                    this._matSnackBar.open(
                        "Error, incidencia no actualizada",
                        "",
                        {
                            verticalPosition: "top",
                            duration: 5000,
                        }
                    );
                }

                console.log("mensaje", err.error.mensaje);
            }
        );
    }
}
