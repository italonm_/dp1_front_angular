import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { fuseAnimations } from "@fuse/animations";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MessageBoxComponent } from "app/shared/message-box/message-box.component";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IncidenteModel } from "app/models/incidente.model";
import { IncidenteService } from "app/services/incidente.service";
import { IncidenteEmpFormComponent } from "./incidente-emp-form/incidente-emp-form.component";

@Component({
    selector: "app-incidentes-emp",
    templateUrl: "./incidentes-emp.component.html",
    styleUrls: ["./incidentes-emp.component.scss"],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None,
})
export class IncidentesEmpComponent implements OnInit {
    public dataSource = new MatTableDataSource<any>();
    visible: boolean;
    dialogRef: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator)
    set paginator(v: MatPaginator) {
        this.dataSource.paginator = v;
    }
    //falta cambiar
    columnNames = [
        "beneficiario",
        "descripcion",
        "motivoIncidencia",
        "fecha",
        "editar",
        "eliminar",
    ];
    idEmpleado: string;
    lugarCobro: any;

    constructor(
        private incidenteService: IncidenteService,
        private router: Router,
        private _matDialog: MatDialog,
        private _matSnackBar: MatSnackBar
    ) {
        this.idEmpleado = localStorage.getItem("usuarioId");
        this.lugarCobro = localStorage.getItem("agenciaId");
    }

    ngOnInit(): void {
        this.visible = false;
        this.incidenteService.listar(this.lugarCobro).subscribe((res) => {
            console.log(res);
            this.visible = true;
            this.dataSource.data = res;
            this.dataSource.sort = this.sort;
        });
    }
    public doFilter = (value: string) => {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    };

    crear(): void {
        this.dialogRef = this._matDialog.open(IncidenteEmpFormComponent, {
            panelClass: "incidente-emp-form-dialog",
            data: {
                action: "nuevo",
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    editar(id: number): void {
        this.dialogRef = this._matDialog.open(IncidenteEmpFormComponent, {
            panelClass: "incidente-emp-form-dialog",
            data: {
                action: "editar",
                id: id,
            },
        });
        this.dialogRef.afterClosed().subscribe((result) => {
            this.ngOnInit();
        });
    }

    eliminar(incidencia: IncidenteModel): void {
        this.dialogRef = this._matDialog.open(MessageBoxComponent, {
            panelClass: "confirm-form-dialog",
            data: {
                titulo: "Eliminar incidencia: " + incidencia.descripcion,
                subTitulo: "¿Está seguro de eliminar la incidencia?",
                botonAceptar: "Si",
                botonCancelar: "No",
            },
        });

        this.dialogRef.afterClosed().subscribe((result) => {
            if (localStorage.getItem("eliminar") === "S") {
                this.incidenteService.eliminar(incidencia, 2).subscribe(
                    (data) => {
                        this._matSnackBar.open("Se eliminó la incidencia", "", {
                            verticalPosition: "top",
                            horizontalPosition: "center",
                            duration: 5000,
                        });
                        this.ngOnInit();
                    },
                    (err) => {
                        console.log("err", err);
                        this._matSnackBar.open(
                            "Error, Incidencia no eliminada",
                            "",
                            {
                                verticalPosition: "top",
                                duration: 5000,
                            }
                        );
                    }
                );
            }
            localStorage.setItem("eliminar", "N");
        });
    }
}
