import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MessageBoxComponent {

  titulo = this._data.titulo;
  subTitulo = this._data.subTitulo;
  botonAceptar = this._data.botonAceptar;
  botonCancelar = this._data.botonCancelar;

  constructor(
    public matDialogRef: MatDialogRef<MessageBoxComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any
  ) { }

  aceptar(): void {
    localStorage.setItem("eliminar", 'S');
    this.matDialogRef.close();
  }

}
