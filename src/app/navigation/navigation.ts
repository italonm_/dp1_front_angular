import { FuseNavigation } from "@fuse/types";

export const navigation: FuseNavigation[] = [
    {
        id: "banco",
        title: "Banco",
        type: "item",
        icon: "account_balance",
        url: "/admin/bancos",
    },
    {
        id: "empleados",
        title: "Empleados",
        type: "item",
        icon: "groups",
        url: "/admin/empleados",
    },
    {
        id: "beneficiarios",
        title: "Beneficiarios",
        type: "item",
        icon: "groups",
        url: "/admin/beneficiarios",
    },
    {
        id: "cronograma",
        title: "Cronograma",
        type: "item",
        icon: "today",
        url: "/admin/cronograma",
    },
    {
        id: "reportes",
        title: "Reportes",
        type: "item",
        icon: "bar_chart",
        url: "/admin/reportes",
    },
    {
        id: "distritos",
        title: "Casos Covid",
        type: "item",
        icon: "place",
        url: "/admin/distritos",
    },
    {
        id: "incidentes",
        title: "Incidentes",
        type: "item",
        icon: "report_problem",
        url: "/admin/incidentes",
    },
];
