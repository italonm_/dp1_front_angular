import { FuseNavigation } from "@fuse/types";

export const navigationEmpleado: FuseNavigation[] = [
    {
        id: "beneficiarios",
        title: "Beneficiarios",
        type: "item",
        icon: "groups",
        url: "/empleado/beneficiarios",
    },
    {
        id: "incidentes",
        title: "Incidentes",
        type: "item",
        icon: "report_problem",
        url: "/empleado/incidentes",
    },
];
